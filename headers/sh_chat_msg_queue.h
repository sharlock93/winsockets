#ifndef SH_CHAT_MSG_QUEUE
#define SH_CHAT_MSG_QUEUE

typedef struct msg_queue {
    chat_msg* queue;
    int size;
    int capacity;
    int head;
    int back;
    HANDLE mutex;
    HANDLE thread;
} msg_queue;

void sh_grow_queue(msg_queue* q);
void sh_push_msg(msg_queue* q, chat_msg* msg);
chat_msg* sh_pop_msg(msg_queue* q);


DWORD handle_msgs(LPVOID param);
#endif
