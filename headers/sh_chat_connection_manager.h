#ifndef SH_CHAT_CON_MAN
#define SH_CHAT_CON_MAN

#include "sh_chat_types.h"
#include "sh_chat_db.h"
#include "sh_chat_msg_queue.h"

#define GROW_BY_MLT 2

typedef enum {
    CLOSED, //  - CLOSE -> socket is close
    OPEN,   //  - OPEN -> socket is open and ready to receive data ? chat_user is valid
    ROOM_INIT, // - ROOM_INIT, user is identified but has no room yet
    INIT,   //  - INIT -> SOCKET is open but chat_user hasn't been inited? inited meaning confirmation of user and password in the future 
} connection_state;

typedef struct connection_handle {
    struct sockaddr_storage addr;
    int con_len; 
    SOCKET socket;
    chat_user user;
    connection_state state;
} connection_handle;

typedef struct socket_manager {
    connection_handle* cons; //con men?
    msg_queue m_queue; //maybe more queues?
    HANDLE thread;
    HANDLE con_mutex;
    long thread_id; //do we need this? if we have the handle

    PGconn* db_connection;
    int capacity;
    int size;
} socket_manager;


SOCKET init_winsock( WORD s_ver, WSADATA* wsa_data, char* ip, char* port);

socket_manager* sh_make_socket_manager(int init_capacity, int msg_capacity);
void sh_sock_add_handle(socket_manager* mngr, connection_handle* handle);
void sh_grow_socket_cap(socket_manager* mngr);
DWORD handle_connection(LPVOID params);

#endif
