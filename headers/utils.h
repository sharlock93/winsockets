#ifndef SH_UTILS_H
#define SH_UTILS_H
#include <stdint.h>
#include <Windows.h>

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;


typedef int8_t  int8;
typedef int16_t int16;
typedef int32_t int32;

typedef int16_t FWord;
typedef uint16_t uFWord;
typedef int16_t F2Dot14;
typedef int32_t Fixed;

typedef int16_t shortFrac;
typedef int64_t longDateTime;

typedef float   f32;

#define UInt16 uint16
#define Offset16 uint16
#define Offset32 uint32

#endif
