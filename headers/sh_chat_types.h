#ifndef SH_CHAT_TYPES
#define SH_CHAT_TYPES

typedef int64_t sh_timestamp;

typedef struct chat_user  {
    int user_id;
    int username_len;
    int room_id;
    unsigned char username[128];
} chat_user;

typedef struct chat_msg {
    char* msg;
    int length;
    int user_id;
    sh_timestamp timestamp;
    int db_stored;
} chat_msg;

#define true 1
#define false 0

#define MAX_USERNAME_LEN 128
#define MAX_SOCKET_PER_THREAD 1000

#endif
