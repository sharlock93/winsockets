#ifndef SH_CHAT_DB
#define SH_CHAT_DB

#include <libpq-fe.h>

char* prints_stat[] = {
    "connection is okay",
    "connection is not okay",
    "connection is starting",
    "connection is made",
    "connection is waiting for a response",
    "connection needs auth",
    "connection is setting envs",
    "connection needs ssl startup",
    "connection is needed",
    "connection is writable check",
    "connection is consumable"
};

void print_stat_con(PGconn* con);
void sh_store_db_msg(PGconn* con, chat_msg* msg, chat_user* user);
void sh_get_db_msg(PGconn* con);
void sh_get_db_user(PGconn* con, char* username, chat_user* user);

#include "sh_chat_psql_types.h"

#endif
