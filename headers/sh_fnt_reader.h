#ifndef SH_FNT_READER
#define SH_FNT_READER
#define TOKEN_LEN(token) (token->t_end - token->t_start)
#define PNT2PXL(pt) (pt)

typedef enum {
    NOTHING,
    INFO,
    COMMON,
    PAGE,
    CHAR_ID,
    CHAR_FNT_INFO
} token_type;

typedef struct sh_token {
    uint8 *t_start;
    uint8 *t_end;
    token_type type;
} sh_token;

typedef struct fnt_info {
    char *face;
    int32 size;
    uint8 bold;
    uint8 italic;
    char *charset;
    uint8 unicode;
    int32 stretchH;
    uint8 smooth;
    uint8 aa;
    int4 padding;
    int2 spacing;
    uint8 outline;
} fnt_info;

typedef struct fnt_common {
    int32 lineHeight;
    int32 base;
    int32 scaleW;
    int32 scaleH;
    int32 pages;
    int32 packed;
    int32 alphaChnl;
    int32 redChnl;
    int32 greenChnl;
    int32 blueChnl;
} fnt_common;

typedef struct fnt_page_inf {
    uint32 id;
    char *file;
} fnt_page_inf;

typedef struct fnt_chars {
    uint32 count;
} fnt_chars;

typedef struct fnt_char {
    int32 id;
    int32 x;
    int32 y;
    int32 width;
    int32 height;
    int32 xoffset;
    int32 yoffset;
    int32 xadvance;
    int32 page;
    int32 chnl;
	int32 desc;
} fnt_char;

typedef struct sh_fnt {
    fnt_info info;
    fnt_common common;
    fnt_page_inf page;
    fnt_chars chars;
    fnt_char characters[256]; //@Note(sharo): cover the first 256 characters
} sh_fnt;

float sh_get_scale_for_pixel(sh_fnt *font, int font_size);
uint8* get_nextline(uint8 *current_pos, uint32 mem_size);
uint8* get_line_end(uint8 *current_pos, uint32 mem_size);
uint8* skip_to_str_end(uint8 *quote_start);
uint8* move_to_value(uint8 *cur_point, uint8 seperator);

token_type get_token_type(sh_token *token);
sh_token get_next_token(uint8 *pos, uint8 seperator);

void fill_fnt_info(uint8 *start_mem, fnt_info *fntinf);
void fill_common_info(uint8 *start_mem, fnt_common *common);
void fill_fnt_page_info(uint8 *mem, fnt_page_inf *pg_inf);
void fill_fnt_chars(uint8 *mem, fnt_chars *fnt_chars);
void fill_fnt_char(uint8 *mem, sh_fnt *font);
void sh_read_character_descripter_in_memory(sh_fnt *font, uint8 *memory, uint32 size);

#endif
