#include "utils.h"

#define GET_MEM_ADR(var) ((uint8 *)(&var))
#define MOVE_POINTER(p, bytes) ((p) += (bytes))
#define READ_BE(src, dest, size) read_be(src, dest, size); MOVE_POINTER(src, size)
#define BE_READ16(mem) ((mem)[0] << 8 | (mem)[1])

void read_be(const uint8 *memory, uint8 *dest, int byte_count) {
	for(int i = byte_count - 1; i >= 0; --i) {
		dest[byte_count - i - 1] = memory[i];
	}
}

#include "ttf_structs.h"

sh_loca sh_read_loca(const uint8 *mem, int32 num_glyphs, int32 type) {
	sh_loca l = {0};
	l.offsets = (uint32 *) malloc(sizeof(uint32)*(num_glyphs+1));
	if(type == 0) {
		for(int i = 0; i <= num_glyphs; ++i) {
			uint16 temp = 0;
			READ_BE(mem, (uint8 *)&temp, sizeof(uint16));
			l.offsets[i] = (uint32)temp;
		}
	} else {
		for(int i = 0; i <= num_glyphs; ++i) {
			uint32 temp = 0;
			READ_BE(mem, (uint8 *)&temp, sizeof(uint32));
			l.offsets[i] = temp;
		}
	}

	l.n = num_glyphs;
	return l;
}

sh_hmtx sh_read_hmtx(const uint8 *mem, int32 num_long_hor_metric) {
	sh_hmtx new_struct = {0};
	new_struct.glyph_metrics = (sh_hor_metric *)malloc(sizeof(sh_hor_metric)*num_long_hor_metric);
	for(int i = 0; i < num_long_hor_metric; ++i) {
		new_struct.glyph_metrics[i].advance_width = BE_READ16(mem+i*4);
		new_struct.glyph_metrics[i].left_side_bearing = BE_READ16(mem+i*4+2);
	}

	return new_struct;
}

sh_format4 sh_read_format4(const char *mem) {
	const char *start_mem = mem;
	sh_format4 new_struct = {0};
	READ_BE(mem, GET_MEM_ADR(new_struct.format), sizeof(UInt16));
	READ_BE(mem, GET_MEM_ADR(new_struct.length), sizeof(UInt16));
	READ_BE(mem, GET_MEM_ADR(new_struct.language), sizeof(UInt16));
	READ_BE(mem, GET_MEM_ADR(new_struct.segCountX2), sizeof(UInt16));
	new_struct.segCount = new_struct.segCountX2/2;
	READ_BE(mem, GET_MEM_ADR(new_struct.searchRange), sizeof(UInt16));
	READ_BE(mem, GET_MEM_ADR(new_struct.entrySelector), sizeof(UInt16));

	READ_BE(mem, GET_MEM_ADR(new_struct.rangeShift), sizeof(UInt16));
	new_struct.endCode = (uint16 *) malloc(new_struct.segCountX2);
	for(int i = 0; i < new_struct.segCountX2/2; ++i) {
		read_be(mem, (uint8*)(new_struct.endCode+i), sizeof(uint16));
		mem += 2;
	}
	READ_BE(mem, GET_MEM_ADR(new_struct.reservedPad), sizeof(UInt16));
	new_struct.startCode = (UInt16 *) malloc(new_struct.segCountX2);
	for(int i = 0; i < new_struct.segCountX2/2; ++i) {
		READ_BE(mem, (uint8*)(new_struct.startCode+i), sizeof(uint16));
	}
	new_struct.idDelta = (UInt16 *) malloc(new_struct.segCountX2);
	for(int i = 0; i < new_struct.segCountX2/2; ++i) {
		READ_BE(mem, (uint8*)(new_struct.idDelta+i), sizeof(uint16));
	}

	new_struct.idRangeOffset = (UInt16 *) malloc(new_struct.segCountX2);
	for(int i = 0; i < new_struct.segCount; ++i) {
		READ_BE(mem, (uint8*)(new_struct.idRangeOffset+i), sizeof(uint16));
	}

	uint16 remaining_length = new_struct.length - (mem - start_mem);
	new_struct.glyphIdArray = (uint16 *) malloc(remaining_length);
	new_struct.glyphIndexCount = remaining_length/2;
	for(int i = 0; i < remaining_length/2; ++i) {
		READ_BE(mem, (uint8 *)(new_struct.glyphIdArray+i), sizeof(uint16));
	}

	return new_struct;
}

void print_format4(sh_format4 *f4) {
	printf("Format: %d, Length: %d, Language: %d, Segment Count: %d\n", f4->format, f4->length, f4->language, f4->segCountX2/2);
	printf("Search Params: (searchRange: %d, entrySelector: %d, rangeShift: %d)\n",
			f4->searchRange, f4->entrySelector, f4->rangeShift);
	printf("Segment Ranges:\tstartCode\tendCode\tidDelta\tidRangeOffset\n");
	for(int i = 0; i < f4->segCountX2/2; ++i) {
		printf("--------------:\t% 9d\t% 7d\t% 7d\t% 12d\n", f4->startCode[i], f4->endCode[i], f4->idDelta[i], f4->idRangeOffset[i]);
	}

	printf("glphIndex:");
	for(int i = 0; i < f4->glyphIndexCount; ++i) {
		if(i%4 == 0) printf("\n--------------:\t");
		printf("\t% 9d", f4->glyphIdArray[i]);
	}
}

void read_name_table(uint8 *mem, name_table *name,
					table_directory_entry *name_table_info)
{
	uint32 offset = name_table_info->offset;
	uint32 len = name_table_info->length;

	mem += offset;
	uint8 *data_pointer = mem;

	read_be(mem, GET_MEM_ADR(name->format), sizeof(name->format));
	MOVE_POINTER(mem, sizeof(name->format));

	read_be(mem, GET_MEM_ADR(name->count), sizeof(name->count));
	MOVE_POINTER(mem, sizeof(name->count));

	read_be(mem, GET_MEM_ADR(name->string_offset), sizeof(name->string_offset));
	MOVE_POINTER(mem, sizeof(name->string_offset));

	MOVE_POINTER(data_pointer, name->string_offset);

	name->records = HeapAlloc(
			GetProcessHeap(),
			HEAP_ZERO_MEMORY,
			sizeof(name_record)*name->count
			);


	for(int i = 0; i < name->count; ++i) {
		name_record *rec = name->records+i;

		read_be(mem, GET_MEM_ADR(rec->platform_id), sizeof(uint16));
		MOVE_POINTER(mem, sizeof(uint16));

		read_be(mem, GET_MEM_ADR(rec->platform_specific_id), sizeof(uint16));
		MOVE_POINTER(mem, sizeof(uint16));

		read_be(mem, GET_MEM_ADR(rec->language_id), sizeof(uint16));
		MOVE_POINTER(mem, sizeof(uint16));

		read_be(mem, GET_MEM_ADR(rec->name_id), sizeof(uint16));
		MOVE_POINTER(mem, sizeof(uint16));

		read_be(mem, GET_MEM_ADR(rec->length), sizeof(uint16));
		MOVE_POINTER(mem, sizeof(uint16));

		read_be(mem, GET_MEM_ADR(rec->offset), sizeof(uint16));
		MOVE_POINTER(mem, sizeof(uint16));

		rec->data = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, rec->length);

		memcpy(rec->data, data_pointer+rec->offset, rec->length);
	}
}

void print_name_table(name_table *name) {
	printf("name table(c: %d) (\n", name->count);
	for(int i = 0; i < name->count; ++i) {
		name_record *r = name->records + i;
		printf("\tr(pid: %d, nid: %d) data: %.*s\n", r->platform_id, r->name_id,
				r->length, r->data);
	}
	printf(")\n");
}

void read_table_directory(uint8* mem, table_directory_entry *table, int32 num_tables) {
	uint8 *file_start = mem - sizeof(offset_subtable);
	for(int i = 0; i < num_tables; ++i) {
		table_directory_entry *ent = table+i;

		CopyMemory(GET_MEM_ADR(ent->tag), mem, sizeof(ent->tag));
		MOVE_POINTER(mem, sizeof(ent->tag));

		read_be(mem, GET_MEM_ADR(ent->check_sum), sizeof(ent->check_sum));
		MOVE_POINTER(mem, sizeof(ent->check_sum));

		read_be(mem, GET_MEM_ADR(ent->offset), sizeof(ent->offset));
		MOVE_POINTER(mem, sizeof(ent->offset));

		read_be(mem, GET_MEM_ADR(ent->length), sizeof(ent->length));
		MOVE_POINTER(mem, sizeof(ent->length));

		ent->data_ptr = file_start+ent->offset;
	}
}

void read_offset_subtable(uint8 *mem, offset_subtable *subtable) {
	uint8 *t_p = mem;
	read_be(t_p, GET_MEM_ADR(subtable->scaler_type), sizeof(subtable->scaler_type));
	MOVE_POINTER(t_p, sizeof(subtable->scaler_type));

	read_be(t_p, GET_MEM_ADR(subtable->num_tables), sizeof(subtable->num_tables));
	MOVE_POINTER(t_p, sizeof(subtable->num_tables));
	
	read_be(t_p, GET_MEM_ADR(subtable->search_range), sizeof(subtable->search_range));
	MOVE_POINTER(t_p, sizeof(subtable->search_range));

	read_be(t_p, GET_MEM_ADR(subtable->entry_selector), sizeof(subtable->entry_selector));
	MOVE_POINTER(t_p, sizeof(subtable->entry_selector));

	read_be(t_p, GET_MEM_ADR(subtable->range_shift), sizeof(subtable->range_shift));
	MOVE_POINTER(t_p, sizeof(subtable->range_shift));
}


//TODO(sharo): did I break it?
void read_font_directory(font_directory *fontdir) {
	uint8 *mem = fontdir->data;
	
	read_offset_subtable(mem, &fontdir->subtable);
	MOVE_POINTER(mem, sizeof(offset_subtable));

	fontdir->table_directory = malloc(sizeof(table_directory_entry)*fontdir->subtable.num_tables);

	read_table_directory(mem, fontdir->table_directory, fontdir->subtable.num_tables);
	fontdir->entry_count = fontdir->subtable.num_tables;
}

void print_font_dir(table_directory_entry *font_dir, int32 entry_count) {
	printf("font directory: (\n");
	for(int i = 0; i < entry_count; ++i) {
		table_directory_entry *entry = font_dir + i;
		printf("\t entry %d (tag: %.4s, offset: %d, length: %d)\n",
				i+1, entry->tag_c, entry->offset, entry->length);
	}
	printf(")\n");
}


void print_subtable(offset_subtable *subtable) {
	printf("type: %.8x, tnum: %d\n", subtable->scaler_type, subtable->num_tables);
}

void process_cmap(table_directory_entry *table, font_directory *font) {
	font->cmap = sh_read_cmap(table->data_ptr);
	for(int i = 0; i < font->cmap.numTables; ++i) {
		sh_EncodingRecord m = sh_read_EncodingRecord(table->data_ptr+sizeof(sh_cmap)+sizeof(sh_EncodingRecord)*i);
		uint16 format = 0;
 		read_be(table->data_ptr+m.offset, GET_MEM_ADR(format), sizeof(uint16));

		printf("%d %d %d\n", format, m.platformID, m.encodingID);
		//microsoft with BMP only Unicode
		if(format == 4 && m.platformID == 0 && m.encodingID  == 3) {
			font->f4 = sh_read_format4(table->data_ptr+m.offset);
			/* print_format4(&font->f4); */
		}
	}
}

uint16 get_glyph_index(sh_format4 *f4, uint16 char_code) {
	int32 seg_index = 0;
	for(int32 i = 0; i < f4->segCount; ++i) {
		if(f4->endCode[i] >= char_code) {
			seg_index = i;
			break;
		}
	}

	if(char_code < f4->startCode[seg_index]) { return 0; }
		
	uint16 range_offset = char_code - f4->startCode[seg_index];
	if(f4->idRangeOffset[seg_index] == 0) {
		return (char_code+f4->idDelta[seg_index])%65536;
	}
	range_offset += f4->idRangeOffset[seg_index]/2;

	// this shit is here because idRangeOffset is not contiguous with glyphIndexArray as it should be
	uint16 *glyph_index = f4->idRangeOffset+seg_index;
	uint16 *idrange_end = f4->idRangeOffset+f4->segCount;

	uint16 offset_into_glyphIdArray = range_offset - (uint16)(idrange_end - glyph_index);
	uint16 glyph_array_index = *(f4->glyphIdArray + offset_into_glyphIdArray);
	if(glyph_array_index != 0) {
		glyph_array_index = (glyph_array_index + f4->idDelta[seg_index])%65536;
	}

	return glyph_array_index;
}

sh_glyph_offset get_glyph_offset(sh_loca* loc, int32 index, int32 type) {
	sh_glyph_offset data = {0};
	data.offset = type == 1 ? loc->offsets[index] : loc->offsets[index]*2;
	data.length = type == 1 ? loc->offsets[index+1] - data.offset : loc->offsets[index+1]*2 - data.offset;
	return data;
}

sh_glyph_rectangle get_glyph_rectangle(font_directory *font, uint16 char_code) {
	sh_glyph_rectangle rect = {0};
	int32 index = get_glyph_index(&font->f4, char_code);
	sh_glyph_offset char_offset = get_glyph_offset(&font->loca, index, font->head.indexToLocFormat);
	uint8 *glyph_data_start = font->glyph_table+char_offset.offset;
	rect.p1 = (sh_fword_point){BE_READ16(glyph_data_start+2), BE_READ16(glyph_data_start+4)};
	rect.p2 = (sh_fword_point){BE_READ16(glyph_data_start+6), BE_READ16(glyph_data_start+8)};
	return rect;
}



sh_glyph_outline get_glyph_outline(font_directory *font, uint16 char_code) {
	// begin_time_block(__func__);
	sh_glyph_outline glyph_outline = { 0 };
	int32 index = get_glyph_index(&font->f4, char_code);
	glyph_outline.glyph_index = index;
	sh_glyph_offset char_offset = get_glyph_offset(&font->loca, index, font->head.indexToLocFormat);
	uint8 *glyph_data_start = font->glyph_table+char_offset.offset;
	int16 contour_count = BE_READ16(glyph_data_start);

	glyph_outline.p1 = (sh_fword_point){BE_READ16(glyph_data_start+2), BE_READ16(glyph_data_start+4)};
	glyph_outline.p2 = (sh_fword_point){BE_READ16(glyph_data_start+6), BE_READ16(glyph_data_start+8)};

	glyph_outline.contour_count = contour_count;

	if(contour_count > 0) {
		glyph_outline.instruction_length = BE_READ16(glyph_data_start+10+contour_count*2);
		glyph_outline.instructions = (uint8 *)malloc(glyph_outline.instruction_length);
		glyph_outline.contour_last_index = (uint16 *)malloc(2*contour_count);

		for(int i = 0; i < contour_count; ++i) {
			glyph_outline.contour_last_index[i] = BE_READ16(glyph_data_start+10+i*2);
		}

		for(int i = 0; i < glyph_outline.instruction_length; ++i) {
			glyph_outline.instructions[i] = (glyph_data_start+10+contour_count*2+2)[i];
		}
		
		int32 last_point_index = BE_READ16(glyph_data_start+10+(contour_count-1)*2);
		int32 size_of_arrays = last_point_index+1; //last_point_index is 0 based
		glyph_outline.points = (sh_font_point*) malloc(sizeof(sh_font_point)*(size_of_arrays));
		
		uint8 *flag_start = glyph_data_start+10+contour_count*2+2+glyph_outline.instruction_length;
		for(int i = 0; i < size_of_arrays; ++i) {
			glyph_outline.points[i].flag = *(sh_glyph_flag*)flag_start;
			if(glyph_outline.points[i].flag.repeat) {
				++flag_start;
				uint8 repeat_count = *flag_start;
				sh_glyph_flag repeated_item = glyph_outline.points[i].flag;
				while(repeat_count-- > 0) {
					glyph_outline.points[++i].flag = repeated_item;
				}
				flag_start++;
			} else {
				flag_start++;
			}
		}

		uint8 *cords_start = flag_start;
		for(int i = 0; i < size_of_arrays; ++i) {
			sh_glyph_flag flag = glyph_outline.points[i].flag;
			uint8 combined_info = (flag.x_short_vector_pos << 1) | (flag.x_short_vector);
			switch(combined_info) {
				case 2: glyph_outline.points[i].x = i > 0 ? glyph_outline.points[i-1].x : 0; break; //special case
				case 0: glyph_outline.points[i].x = BE_READ16(cords_start) + (i > 0 ? glyph_outline.points[i-1].x : 0); cords_start += 2; break;
				case 1:case 3: {
					int16 current_point = (*cords_start);
					cords_start += 1;
					glyph_outline.points[i].x = (flag.x_short_vector_pos ? current_point : current_point*-1) + (i > 0 ? glyph_outline.points[i-1].x : 0);
				} break;
			}
		}

		for(int i = 0; i < size_of_arrays; ++i) {
			sh_glyph_flag info = glyph_outline.points[i].flag;
			uint8 combined_info = (info.y_short_vector_pos << 1) | (info.y_short_vector);
			switch(combined_info) {
				case 2: glyph_outline.points[i].y = i > 0 ? glyph_outline.points[ i-1].y : 0 ; break; //special case
				case 0: glyph_outline.points[i].y = BE_READ16(cords_start) + (i > 0 ? glyph_outline.points[i-1].y : 0) ; cords_start += 2; break;
				case 1:case 3: {
					int16 current_point = (*cords_start);
					cords_start += 1;
					glyph_outline.points[i].y = (info.y_short_vector_pos ? current_point : current_point*-1) + (i > 0 ? glyph_outline.points[i-1].y : 0);
				} break;
			}
		}
		glyph_outline.points_count = size_of_arrays;
	}

	end_time_block(__func__);
	return glyph_outline;
}

void sh_free_glyph_outline(sh_glyph_outline *outline) {
	free(outline->instructions);
	free(outline->contour_last_index);
	free(outline->points);
}

sh_glyph_metric sh_get_glyph_metric(font_directory *font, sh_glyph_outline *glyph, float scale) {
	sh_hhea* hhea = &font->hhea;
	sh_glyph_metric metric = {0};

	if(hhea->numOfLongHorMetrics == 1) {
		metric.x_advance = font->hmtx.glyph_metrics[0].advance_width*scale;
		metric.left_side_bearing = font->hmtx.glyph_metrics[0].left_side_bearing*scale;
	} else {
		metric.x_advance = font->hmtx.glyph_metrics[glyph->glyph_index].advance_width*scale;
		metric.left_side_bearing = font->hmtx.glyph_metrics[glyph->glyph_index].left_side_bearing*scale;
	}

	return metric;
}

font_directory* sh_init_font(uint8 *memory) {
	uint8 *file = memory;
	//TODO(sharo): should this be in their own place?
	font_directory *font = malloc(sizeof(font_directory));
	font->data = file;
	read_font_directory(font);

	for(int i = 0; i < font->subtable.num_tables; ++i) {
		table_directory_entry *table = font->table_directory + i;
		switch(table->tag) {
			case CMAP:
				process_cmap(table, font);
				break;
			case MAXP:
				font->maxp = sh_read_maxp(table->data_ptr);
				break;
			case HHEA:
				font->hhea = sh_read_hhea(table->data_ptr);
				break;
			case HEAD:
				font->head = sh_read_head(table->data_ptr);
				break;
			case GLYF:
				font->glyph_table = table->data_ptr;
				break;
		}
	}

	for(int i = 0; i < font->subtable.num_tables; ++i) {
		table_directory_entry *table = font->table_directory + i;
		switch(table->tag) {
			case LOCA:
				font->loca = sh_read_loca(table->data_ptr,
						font->maxp.numGlyphs,
						font->head.indexToLocFormat);
				break;
			case HMTX:
				font->hmtx = sh_read_hmtx(table->data_ptr, font->hhea.numOfLongHorMetrics);
				break;
		}
	}
	return font;
}
