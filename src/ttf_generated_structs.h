#ifndef _TTF_GEN_H
#define _TTF_GEN_H
typedef struct sh_cmap {
	uint16 version; // version 
 	uint16 numTables; // numTables 
 } sh_cmap;

typedef struct sh_EncodingRecord {
	uint16  platformID ; // platformID  
 	uint16  encodingID ; // encodingID  
 	Offset32  offset ; // offset  
 } sh_EncodingRecord;

typedef struct sh_glyf {
	int16  numberOfContours ; // numberOfContours  
 	FWord  xMin ; // xMin  
 	FWord  yMin ; // yMin  
 	FWord  xMax ; // xMax  
 	FWord  yMax ; // yMax  
 } sh_glyf;

typedef struct sh_head {
	Fixed  version ; // version  
 	Fixed  fontRevision ; // fontRevision  
 	uint32  checkSumAdjustment ; // checkSumAdjustment  
 	uint32  magicNumber ; // magicNumber  
 	uint16  flags ; // flags  
 	uint16  unitsPerEm ; // unitsPerEm  
 	longDateTime  created ; // created  
 	longDateTime  modified ; // modified  
 	FWord  xMin ; // xMin  
 	FWord  yMin ; // yMin  
 	FWord  xMax ; // xMax  
 	FWord  yMax ; // yMax  
 	uint16  macStyle ; // macStyle  
 	uint16  lowestRecPPEM ; // lowestRecPPEM  
 	int16  fontDirectionHint ; // fontDirectionHint  
 	int16  indexToLocFormat ; // indexToLocFormat  
 	int16  glyphDataFormat ; // glyphDataFormat  
 } sh_head;

typedef struct sh_hhea {
	Fixed  version ; // version  
 	FWord  ascent ; // ascent  
 	FWord  descent ; // descent  
 	FWord  lineGap ; // lineGap  
 	uFWord  advanceWidthMax ; // advanceWidthMax  
 	FWord  minLeftSideBearing ; // minLeftSideBearing  
 	FWord  minRightSideBearing ; // minRightSideBearing  
 	FWord  xMaxExtent ; // xMaxExtent  
 	int16  caretSlopeRise ; // caretSlopeRise  
 	int16  caretSlopeRun ; // caretSlopeRun  
 	FWord  caretOffset ; // caretOffset  
 	int16  reserved1; // reserved1 
 	int16  reserved2; // reserved2 
 	int16  reserved3; // reserved3 
 	int16  reserved4; // reserved4 
 	int16  metricDataFormat ; // metricDataFormat  
 	uint16  numOfLongHorMetrics ; // numOfLongHorMetrics  
 } sh_hhea;

typedef struct sh_maxp {
	Fixed  version ; // version  
 	uint16  numGlyphs ; // numGlyphs  
 	uint16  maxPoints ; // maxPoints  
 	uint16  maxContours ; // maxContours  
 	uint16  maxComponentPoints ; // maxComponentPoints  
 	uint16  maxComponentContours ; // maxComponentContours  
 	uint16  maxZones ; // maxZones  
 	uint16  maxTwilightPoints ; // maxTwilightPoints  
 	uint16  maxStorage ; // maxStorage  
 	uint16  maxFunctionDefs ; // maxFunctionDefs  
 	uint16  maxInstructionDefs ; // maxInstructionDefs  
 	uint16  maxStackElements ; // maxStackElements  
 	uint16  maxSizeOfInstructions ; // maxSizeOfInstructions  
 	uint16  maxComponentElements ; // maxComponentElements  
 	uint16  maxComponentDepth ; // maxComponentDepth  
 } sh_maxp;

typedef struct sh_os_2 {
	uint16 version; // version 
 	int16  xAvgCharWidth ; // xAvgCharWidth  
 	uint16  usWeightClass ; // usWeightClass  
 	uint16  usWidthClass ; // usWidthClass  
 	int16  fsType ; // fsType  
 	int16  ySubscriptXSize ; // ySubscriptXSize  
 	int16  ySubscriptYSize ; // ySubscriptYSize  
 	int16  ySubscriptXOffset ; // ySubscriptXOffset  
 	int16  ySubscriptYOffset ; // ySubscriptYOffset  
 	int16  ySuperscriptXSize ; // ySuperscriptXSize  
 	int16  ySuperscriptYSize ; // ySuperscriptYSize  
 	int16  ySuperscriptXOffset ; // ySuperscriptXOffset  
 	int16  ySuperscriptYOffset ; // ySuperscriptYOffset  
 	int16  yStrikeoutSize ; // yStrikeoutSize  
 	int16  yStrikeoutPosition ; // yStrikeoutPosition  
 	int16  sFamilyClass ; // sFamilyClass  
 	PANOSE  panose ; // panose  
 	uint32  ulCharRange[4] ; // ulCharRange[4]  
 	int8  achVendID[4] ; // achVendID[4]  
 	uint16  fsSelection ; // fsSelection  
 	uint16  fsFirstCharIndex ; // fsFirstCharIndex  
 	uint16  fsLastCharIndex ; // fsLastCharIndex  
 } sh_os_2;

typedef struct sh_RatioRange {
	uint8  bCharSet ; // bCharSet  
 	uint8  xRatio ; // xRatio  
 	uint8  yStartRatio ; // yStartRatio  
 	uint8  yEndRatio ; // yEndRatio  
 } sh_RatioRange;

typedef struct sh_vdmx {
	uint16  version ; // version  
 	uint16  numRecs ; // numRecs  
 	uint16  numRatios ; // numRatios  
 } sh_vdmx;

sh_cmap sh_read_cmap(const char *mem) {
	sh_cmap new_struct = {0};
	READ_BE(mem,GET_MEM_ADR(new_struct.version), sizeof(uint16));
	READ_BE(mem,GET_MEM_ADR(new_struct.numTables), sizeof(uint16));
	return new_struct;
}
sh_EncodingRecord sh_read_EncodingRecord(const char *mem) {
	sh_EncodingRecord new_struct = {0};
	READ_BE(mem,GET_MEM_ADR(new_struct.platformID ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.encodingID ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.offset ), sizeof(Offset32 ));
	return new_struct;
}
sh_glyf sh_read_glyf(const char *mem) {
	sh_glyf new_struct = {0};
	READ_BE(mem,GET_MEM_ADR(new_struct.numberOfContours ), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.xMin ), sizeof(FWord ));
	READ_BE(mem,GET_MEM_ADR(new_struct.yMin ), sizeof(FWord ));
	READ_BE(mem,GET_MEM_ADR(new_struct.xMax ), sizeof(FWord ));
	READ_BE(mem,GET_MEM_ADR(new_struct.yMax ), sizeof(FWord ));
	return new_struct;
}
sh_head sh_read_head(const char *mem) {
	sh_head new_struct = {0};
	READ_BE(mem,GET_MEM_ADR(new_struct.version ), sizeof(Fixed ));
	READ_BE(mem,GET_MEM_ADR(new_struct.fontRevision ), sizeof(Fixed ));
	READ_BE(mem,GET_MEM_ADR(new_struct.checkSumAdjustment ), sizeof(uint32 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.magicNumber ), sizeof(uint32 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.flags ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.unitsPerEm ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.created ), sizeof(longDateTime ));
	READ_BE(mem,GET_MEM_ADR(new_struct.modified ), sizeof(longDateTime ));
	READ_BE(mem,GET_MEM_ADR(new_struct.xMin ), sizeof(FWord ));
	READ_BE(mem,GET_MEM_ADR(new_struct.yMin ), sizeof(FWord ));
	READ_BE(mem,GET_MEM_ADR(new_struct.xMax ), sizeof(FWord ));
	READ_BE(mem,GET_MEM_ADR(new_struct.yMax ), sizeof(FWord ));
	READ_BE(mem,GET_MEM_ADR(new_struct.macStyle ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.lowestRecPPEM ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.fontDirectionHint ), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.indexToLocFormat ), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.glyphDataFormat ), sizeof(int16 ));
	return new_struct;
}
sh_hhea sh_read_hhea(const char *mem) {
	sh_hhea new_struct = {0};
	READ_BE(mem,GET_MEM_ADR(new_struct.version ), sizeof(Fixed ));
	READ_BE(mem,GET_MEM_ADR(new_struct.ascent ), sizeof(FWord ));
	READ_BE(mem,GET_MEM_ADR(new_struct.descent ), sizeof(FWord ));
	READ_BE(mem,GET_MEM_ADR(new_struct.lineGap ), sizeof(FWord ));
	READ_BE(mem,GET_MEM_ADR(new_struct.advanceWidthMax ), sizeof(uFWord ));
	READ_BE(mem,GET_MEM_ADR(new_struct.minLeftSideBearing ), sizeof(FWord ));
	READ_BE(mem,GET_MEM_ADR(new_struct.minRightSideBearing ), sizeof(FWord ));
	READ_BE(mem,GET_MEM_ADR(new_struct.xMaxExtent ), sizeof(FWord ));
	READ_BE(mem,GET_MEM_ADR(new_struct.caretSlopeRise ), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.caretSlopeRun ), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.caretOffset ), sizeof(FWord ));
	READ_BE(mem,GET_MEM_ADR(new_struct.reserved1), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.reserved2), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.reserved3), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.reserved4), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.metricDataFormat ), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.numOfLongHorMetrics ), sizeof(uint16 ));
	return new_struct;
}
sh_maxp sh_read_maxp(const char *mem) {
	sh_maxp new_struct = {0};
	READ_BE(mem,GET_MEM_ADR(new_struct.version ), sizeof(Fixed ));
	READ_BE(mem,GET_MEM_ADR(new_struct.numGlyphs ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.maxPoints ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.maxContours ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.maxComponentPoints ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.maxComponentContours ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.maxZones ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.maxTwilightPoints ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.maxStorage ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.maxFunctionDefs ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.maxInstructionDefs ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.maxStackElements ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.maxSizeOfInstructions ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.maxComponentElements ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.maxComponentDepth ), sizeof(uint16 ));
	return new_struct;
}
sh_os_2 sh_read_os_2(const char *mem) {
	sh_os_2 new_struct = {0};
	READ_BE(mem,GET_MEM_ADR(new_struct.version), sizeof(uint16));
	READ_BE(mem,GET_MEM_ADR(new_struct.xAvgCharWidth ), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.usWeightClass ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.usWidthClass ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.fsType ), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.ySubscriptXSize ), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.ySubscriptYSize ), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.ySubscriptXOffset ), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.ySubscriptYOffset ), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.ySuperscriptXSize ), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.ySuperscriptYSize ), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.ySuperscriptXOffset ), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.ySuperscriptYOffset ), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.yStrikeoutSize ), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.yStrikeoutPosition ), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.sFamilyClass ), sizeof(int16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.panose ), sizeof(PANOSE ));
	READ_BE(mem,GET_MEM_ADR(new_struct.ulCharRange[4] ), sizeof(uint32 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.achVendID[4] ), sizeof(int8 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.fsSelection ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.fsFirstCharIndex ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.fsLastCharIndex ), sizeof(uint16 ));
	return new_struct;
}
sh_RatioRange sh_read_RatioRange(const char *mem) {
	sh_RatioRange new_struct = {0};
	READ_BE(mem,GET_MEM_ADR(new_struct.bCharSet ), sizeof(uint8 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.xRatio ), sizeof(uint8 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.yStartRatio ), sizeof(uint8 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.yEndRatio ), sizeof(uint8 ));
	return new_struct;
}
sh_vdmx sh_read_vdmx(const char *mem) {
	sh_vdmx new_struct = {0};
	READ_BE(mem,GET_MEM_ADR(new_struct.version ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.numRecs ), sizeof(uint16 ));
	READ_BE(mem,GET_MEM_ADR(new_struct.numRatios ), sizeof(uint16 ));
	return new_struct;
}
#endif
