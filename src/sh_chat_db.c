#include "sh_chat_db.h"

void print_stat_con(PGconn* con) {
    static int check_inter = 0;
    printf("%d:%s\n", check_inter++,prints_stat[PQstatus(con)]);
}

PGconn* sh_init_db_con(char** init_keys, char** init_values) {

    //Todo(sharo): this needs to not use WSAPoll, not backwards compatible
    PGconn* db_connection = PQconnectStartParams(init_keys, init_values, 0);
    int db_socket = PQsocket(db_connection);

    WSAPOLLFD p[] = {{ db_socket, POLLWRNORM, 0 }};
    printf("connecting to db...");

    while(PQstatus(db_connection) != CONNECTION_OK && PQstatus(db_connection) != CONNECTION_BAD) {
	int polices =  PQconnectPoll(db_connection);
	switch(polices)  {
	    case PGRES_POLLING_FAILED:
		break;
	    case PGRES_POLLING_READING:
		p[0].events = POLLRDNORM;
		break;
	    case PGRES_POLLING_WRITING:
		p[0].events = POLLWRNORM;
		break;
	    case PGRES_POLLING_OK:
		break;
	}

	if(polices == PGRES_POLLING_OK) break;
	int poll_res = WSAPoll(p, 1, 0);
	while(poll_res != 1) {
	    poll_res = WSAPoll(p, 1, 0);
	}
    }
    printf("done\n");

    //if(db_connection == NULL) return -1;
    return db_connection;

}


void sh_store_db_msg(PGconn* con, chat_msg* msg, chat_user* user) {
#if 1
    static char buffer[12];
    snprintf(buffer, 12, "%d", user->user_id);
    char* val[] = {
	buffer,
	msg->msg,
	"now"
    };

    PGresult* res = PQexecPrepared(con, "sh_chat_stmt_ins", 3, val, NULL, NULL, 0);

    printf("%s\n", PQerrorMessage(con));
#endif
}


void sh_get_db_user(PGconn* con, char* username, chat_user* user) {

    char* val[] = {
	username
    };
    
    PGresult* res = PQexecPrepared(con, "sh_chat_stmt_get", 1, val, NULL, NULL, 0);    


    printf("%d %s", PQntuples(res), username);
    if(res == NULL || (PQntuples(res) == 0)) { 
	printf("%s\n", PQerrorMessage(con));
	user->user_id = -1;
	return;
    }
    printf("res");

    user->user_id = strtod(PQgetvalue(res, 0, 0), NULL);
    char* db_username = PQgetvalue(res, 0, 1);
    memcpy(user->username, db_username, strlen(db_username)); //don't include the new line
    user->username_len = strlen(db_username); //no new line
    // user->username[bytes_gotten] = '\0';
}
