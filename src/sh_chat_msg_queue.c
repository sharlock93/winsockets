#include "sh_chat_msg_queue.h"

void sh_grow_queue(msg_queue* q) {
    assert(q->size >= q->capacity);
    void* old_mem = q->queue;
    WaitForSingleObject(q->mutex, INFINITE);
    void* new_mem = HeapReAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, old_mem, sizeof(chat_msg)*q->capacity*GROW_BY_MLT);
    if(new_mem != NULL) {
	q->queue = (chat_msg*) new_mem;
	q->capacity *= GROW_BY_MLT;
    }
    ReleaseMutex(q->mutex);
}

void sh_push_msg(msg_queue* q, chat_msg* msg) {
    if(q->size >= q->capacity) {
	printf("growing up");
	sh_grow_queue(q);
    }

    printf("%d:%d | %d:%d\n", q->size, q->capacity, q->head, q->back);

    chat_msg* loc = &q->queue[q->head++];

    loc->msg = (char*) HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(char)*msg->length+1);

    CopyMemory(loc->msg, msg->msg, msg->length);
    loc->length = msg->length;
    loc->timestamp = msg->timestamp;
    loc->user_id = msg->user_id;
    loc->msg[loc->length] = '\0';


    q->size++;
    q->head = q->head%q->capacity;
}

chat_msg* sh_pop_msg(msg_queue* q) {
    /* if(q->size <= 0) return NULL; */
    if(( q->back == q->head )) return NULL;

    
    chat_msg* ret = &q->queue[q->back++];
    q->back = q->back % q->capacity;
    q->size--;

    return ret;
}

//TODO(sharo): threading decouple? maybe?
DWORD handle_msgs(LPVOID param) {
    socket_manager* mngr = (socket_manager*) param;
    msg_queue* q = (msg_queue*)&mngr->m_queue;
    chat_msg* m = NULL;
    while(true) {
	while(m = sh_pop_msg(q)) {
	    printf("%s", m->msg);

	    for(int i = 0; i < mngr->capacity; ++i) {
		connection_handle* con = &mngr->cons[i];
		if(con->state == OPEN) {
		    send(con->socket, m->msg, m->length, 0);
		}

	    }
	    HeapFree(GetProcessHeap(), 0, m->msg);
	}

	Sleep(10);
    }

}


