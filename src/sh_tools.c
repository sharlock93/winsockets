#ifndef SH_TOOLS
#define SH_TOOLS
typedef struct bhdr {
	int size;
	int cap;
} bhdr;

#define buf__hdr(b) ((bhdr*)(((int*)b) - 2))

#define buf_end(b) ((b) + buf_len(b))
#define buf_len(b) ((b) ? buf__hdr(b)->size: 0)
#define buf_cap(b) ((b) ? buf__hdr(b)->cap: 0)
#define buf_fit(b, n) ((n) <= buf_cap(b) ? 0 : ((b) = buf__grow(b, buf_len(b) + (n), sizeof(*(b)), __func__, __LINE__)))
#define buf_push(b, ...) ( buf_fit((b), 1 + buf_len(b)), (b)[buf__hdr(b)->size++] = (__VA_ARGS__))
#define buf_free(b) ((b) ? (free(buf__hdr(b)), (b) = NULL): 0)
#define buf_clear(b) ((b) ? buf__hdr(b)->size = 0: 0)

void* buf__grow(void *buf, int items, size_t item_size, char* func_name, int line) {
	size_t new_cap = 1 + 2*buf_cap(buf);
	assert(new_cap >= items);
	size_t new_size = sizeof(int)*2 + new_cap*item_size;
	bhdr *nhdr = NULL;
	if(buf) {
		nhdr = realloc(buf__hdr(buf), new_size);
	} else {
		nhdr = malloc(new_size);
		nhdr->size = 0;
	}

	nhdr->cap = new_cap;
	return ((int*)nhdr) + 2;
}

FILETIME sh_get_file_last_write(char *filename) {
	WIN32_FILE_ATTRIBUTE_DATA file_attrib;
	int success = GetFileAttributesExA(filename, GetFileExInfoStandard, &file_attrib);

	if(success == 0) {
		printf("suomething is fucke\n");
	}

	assert(success == true);

	return file_attrib.ftLastWriteTime;

}

bool sh_check_file_changed(char *filename, FILETIME *last_write_time, FILETIME *last_write_time_out) {
	WIN32_FILE_ATTRIBUTE_DATA file_attrib;
	bool success = GetFileAttributesExA(filename, GetFileExInfoStandard, &file_attrib);
	int32 last_error = GetLastError();
	
	bool result = 0;
	if(CompareFileTime(last_write_time, &file_attrib.ftLastWriteTime) == -1) {
		result = 1;
	}

	if(success && last_write_time_out != NULL) {
		*last_write_time_out = file_attrib.ftLastWriteTime;
	}
	
	return result && success;
}

#endif
