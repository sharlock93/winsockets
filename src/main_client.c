#include <stdio.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>

#define TEMP_BUFFER_SIZE 256
#define HTTP_PORT "6969"
char* server_addr = "localhost";
struct addrinfo* server_connection;

int con_socket;

void init_sockets() {
	WSADATA data;
	int initialize_sockets = WSAStartup(MAKEWORD(2, 2), &data);

	if(initialize_sockets != 0) return;

	struct addrinfo hints = {0};	
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	int error = getaddrinfo(server_addr, HTTP_PORT, &hints, &server_connection);

	con_socket = socket(server_connection->ai_family,
			server_connection->ai_socktype,
			server_connection->ai_protocol);

	printf("made socket in client (%d)\n", con_socket);

	error = connect(con_socket, server_connection->ai_addr, server_connection->ai_addrlen);

	struct sockaddr_in* sock_in = (struct sockaddr_in*) server_connection->ai_addr;

	printf("we have connected to the server (%s)\n", inet_ntoa(sock_in->sin_addr));

	if(error != 0) return;
}

DWORD handle_msg(LPVOID param) {
	SOCKET* soc = (SOCKET*) param;
	char buffer[256];
	while(1) {
		int bytes_gotten = recv(con_socket, buffer, 256, 0);
		if(bytes_gotten == SOCKET_ERROR) {
			Sleep(10);
			continue;
		}
		printf("%.*s %d\nenter: ", bytes_gotten, buffer,bytes_gotten);
	}	
}


typedef enum {
	CHAT_MSG,
	CHAT_CMD
} INPUT_TYPE;

INPUT_TYPE shch_check_input_type(char* input) {

	switch(input[0]) {
		case '/':
			return CHAT_CMD;
		default:
			return CHAT_MSG;
	}
}

void get_input_from_cmd(char* input, size_t* msg_length) {
	char temp[TEMP_BUFFER_SIZE];
	int more_input = 0;
	input = NULL;

	do{
		int got_in = 0;
		fgets(temp, sizeof(temp), stdin);

		if(got_in == 0) {
			input = NULL;
			*msg_length = -1;
			return;
		}

		int cur_len = strlen(temp);
		if(cur_len < TEMP_BUFFER_SIZE - 1 && input == NULL) {
			input = (char*) malloc(sizeof(char)*TEMP_BUFFER_SIZE);
			*msg_length = cur_len;
		} else if(input) {

		}

	} while(more_input);

}

LRESULT CALLBACK sh_client_proc(HWND hwnd, UINT u_msg, WPARAM w_param, LPARAM l_param) {

	switch(u_msg) {
		case WM_CREATE:
			CreateWindow("EDIT",
					NULL,
					WS_CHILD | WS_VISIBLE | WS_VSCROLL | ES_MULTILINE, 
					0, 400, 500,50,
					hwnd,
					NULL,
					NULL,
					NULL
					);
			break;
		case WM_PAINT:
			PAINTSTRUCT ps;
			char* meo = "Hello world";
			HDC hdc = BeginPaint(hwnd, &ps);

			TextOut(hdc, 5, 5, meo, strlen(meo));
			EndPaint(hwnd, &ps);
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		case WM_CHAR:
			char buf[256];
			snprintf(buf, 256, "%c", (char) w_param);
			MessageBox(NULL, buf, NULL, MB_OK);
			break;
		default:
			return DefWindowProc(hwnd, u_msg, w_param, l_param);
	}


	return 0;
}

void init_window(HINSTANCE h_inst) {
	WNDCLASS wclass;

	wclass.style = CS_CLASSDC | CS_HREDRAW | CS_OWNDC;
	wclass.lpfnWndProc = sh_client_proc;
	wclass.cbClsExtra = 0;
	wclass.cbWndExtra = 0;
	wclass.hInstance = h_inst;
	wclass.hIcon = NULL;
	wclass.hCursor = NULL;
	wclass.hbrBackground = NULL;
	wclass.lpszClassName = "sh_chat_client";

	RegisterClass(&wclass);

	HWND mw = CreateWindow(
			"sh_chat_client",
			"Sharo Chat Client",
			WS_BORDER  | WS_OVERLAPPED | WS_VISIBLE,
			0, 0, 500, 500, //init pos and size
			NULL, //Parent
			NULL, //menu
			h_inst,
			NULL
			);

}

int CALLBACK WinMain(HINSTANCE h_inst, HINSTANCE h_p_inst, LPSTR cmd_params, int cmd_show) {

	init_window(h_inst);



	MSG msg;
	while(GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return (int) msg.wParam;
}
