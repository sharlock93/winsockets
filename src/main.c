
#include <stdio.h>
#include <stdint.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <assert.h>
#include <stdint.h>

#include "sh_chat_connection_manager.c"
#include "sh_chat_msg_queue.c"
#include "sh_chat_db.c"

#define SH_DEBUG
// #define MAX_MSG_LEN 1024

char* params[] = {
	"host",
	"hostaddr",
	"dbname",
	"user",
	"password",
	NULL
};

char* values[] = {
	"localhost",
	"127.0.0.1",
	"postgres",
	"postgres",
	"sharo",
	NULL
};

char* store_sql = "insert into sh_chat_msg(user_id, chat_msg, timestamp) values($1, $2, $3)";
char* get_sql = "select * from sh_chat_users where username = $1";
PGresult* prep_ins_stmt = NULL;


WSADATA wsa_data;

SOCKET con_sock = INVALID_SOCKET;

int main(int argc, char** argv) {
#if 1
	con_sock = init_winsock(MAKEWORD(2, 2), &wsa_data, "localhost", "6969");
	int listen_socket = listen(con_sock, MAX_SOCKET_PER_THREAD);
	if(listen_socket == SOCKET_ERROR) goto cleanup;
	printf("con_sock: %lld", con_sock);

	Oid param_types[] = {INT4OID, VARCHAROID, TIMESTAMPOID, VARCHAROID};
	socket_manager* manager = sh_make_socket_manager(1, 10);
	manager->db_connection = sh_init_db_con(params, values);
	prep_ins_stmt = PQprepare(manager->db_connection, "sh_chat_stmt_ins", store_sql, 3, param_types);

	printf("%s\n", PQerrorMessage(manager->db_connection));

	PQprepare(manager->db_connection, "sh_chat_stmt_get", get_sql, 1, NULL);
	printf("%s\n", PQerrorMessage(manager->db_connection));

	while(true) {
		connection_handle* handle = (connection_handle*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(connection_handle));
		handle->con_len = sizeof(handle->addr);
		handle->socket = accept(con_sock, (struct sockaddr *)&handle->addr, &handle->con_len);
		handle->state = INIT;

		if(handle->socket != INVALID_SOCKET) {
			int none_block_io = 1;
			ioctlsocket(handle->socket, FIONBIO, &none_block_io);
			sh_sock_add_handle(manager, handle);
		} else {
			HeapFree(GetProcessHeap(), 0, handle);
		}
	}

cleanup:
	WSACleanup();
	return 0;
#endif
}
