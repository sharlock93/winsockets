#include <windows.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include "utils.c"

typedef enum token_type {
	NAME,
	TYPE,
	DESCRIPTION,
	START_STRUCT,
	END_STRUCT,
	MODS
} token_type;

typedef struct sh_token {
	char *name;
	token_type type;
} sh_token;

sh_token gl_token;
uint8 *file;
FILE *generated_file;

bool is_whitespace(const char *c) {
	if(*c == ' ' || *c == '\n') return true;
	return false;
}

bool is_eof(const char *c) {
	return *c == '\0';
}

bool is_delimit(const char *c) {
	if(*c == '\t' || *c == '@') return true;
	return false;
}

sh_token* printf_struct_start(const char *struct_name) {
	sh_token *start_struct = NULL;
	buf_push(start_struct, ((sh_token){ .name = strdup(struct_name), .type = START_STRUCT }));
	buf_push(start_struct, ((sh_token){ .name = "START_STRUCT", .type = START_STRUCT }));
	buf_push(start_struct, ((sh_token){ .name = "START_STRUCT", .type = START_STRUCT }));

	return start_struct;
}

sh_token* printf_struct_end() {
	sh_token *end_struct = NULL;
	buf_push(end_struct, ((sh_token){ .name = "END_STRUCT", .type = END_STRUCT }));
	buf_push(end_struct, ((sh_token){ .name = "END_STRUCT", .type = END_STRUCT }));
	buf_push(end_struct, ((sh_token){ .name = "END_STRUCT", .type = END_STRUCT }));
	return end_struct;
}

void print_token(sh_token *token) {
	printf("token(n: %s, t: %d)\n", token->name, token->type);
}

void print_field(sh_token fields[3]) {
	printf("field(t: %s, n: %s, d: %s)\n", fields[0].name, fields[1].name, fields[2].name);
}

void print_struct_field(sh_token fields[3]) {
	fprintf(generated_file, "\t%s %s; // %s \n ", fields[0].name, fields[1].name, fields[1].name);
}

void open_function(const char *struct_name) {
	fprintf(generated_file,
			"sh_%s sh_read_%s(const char *mem) {\n\tsh_%s new_struct = {0};\n",
			struct_name, struct_name, struct_name);
}

void print_read_field_be(sh_token *field) {
	fprintf(generated_file, 
			"\tREAD_BE(mem,GET_MEM_ADR(new_struct.%s), sizeof(%s));\n",
			field[1].name, field[0].name, field[0].name);
}

void close_function() {
	fprintf(generated_file, "\treturn new_struct;\n}\n");
}
void open_struct(const char *struct_name) {
	fprintf(generated_file, "typedef struct sh_%s {\n", struct_name);
}

void close_struct( const char *struct_name ) {
	fprintf(generated_file, "} sh_%s;", struct_name);
}


void eat_whitespace() {
	while(is_whitespace(file)) file++;
}

sh_token make_token(token_type type) {
	sh_token token = {0};
	size_t str_length = file - gl_token.name;

	token.name = (char*) malloc(str_length+1);
	token.type = type;

	memcpy(token.name, gl_token.name, str_length);
	token.name[str_length] = 0;
	return token;
}

sh_token read_desc_token() {
	assert(file != NULL);
	eat_whitespace();
	gl_token.name = file;
	// while(*++file != '\n' && *file != '\0' && );
	while(*++file != '\n' && !is_eof(file) && !is_delimit(file));
	sh_token t = make_token(DESCRIPTION);
	return t;
}

sh_token read_specs() {
	assert(file[0] == '@');
	assert(strncmp(file, "@defines", strlen("@defines")) == 0);
	gl_token.name = file;
	while(*++file != '\n');

	return make_token(MODS);
}

sh_token next_token(token_type type) {
	assert(file != NULL);
	eat_whitespace();
	gl_token.name = file;
	sh_token t = {0};

top:
	switch(file[0]) {
		case ' ':
			file++;
			goto top;
			break;
		case '\t':case '\n':
			t =  make_token(type);
			file++;
			break;
		case '\0':
			t =  make_token(type);
			break;
		case '@':
			printf("%.*s\n", 24, file-12);
			return read_specs();
			break;
		default: {
			file++;
			goto top;
		} break;
	}

	return t;
}

void parse_line(sh_token fields[3]) {
	eat_whitespace();
	fields[0] = next_token(NAME);
	fields[1] = next_token(TYPE);
	fields[2] = next_token(DESCRIPTION);
	eat_whitespace();
}


const char **files = NULL;
char* clean_file_name(const char *file) {
	int length = strlen(file);
	bool has_extension = false;
	int extension_start_index = 0;
	for(int i = length; i >= 0; --i) {
		if(file[i] == '.') {
			has_extension = true;
			extension_start_index = i;
			break;
		}
	}

	if(extension_start_index != 0) {
		char *m = (char *)malloc(extension_start_index);
		strncpy(m, file, extension_start_index);
		m[extension_start_index] = '\0';
		return m;
	}
	return NULL;
}

char** list_files(const char *dir_name) {
	WIN32_FIND_DATA x = {0};
	HANDLE files = FindFirstFile(dir_name, &x);
	char **file_names = NULL;

	if(files != INVALID_HANDLE_VALUE) {
		do {
			if(x.cFileName[0] == '.') continue;
			buf_push(file_names, strdup(x.cFileName));
		} while(FindNextFile(files, &x));
	} else {
		printf("fuck \n");
	}
	return file_names;
}



int main(int argc, char** argv) {

	char **files = list_files("structs/*");
	char temp_buff[64] = {0};	

	generated_file = fopen("../src/ttf_generated_structs.h", "w");
	sh_token **fields = NULL;
	uint8 *temp_file = NULL;
	fprintf(generated_file, "#ifndef _TTF_GEN_H\n#define _TTF_GEN_H\n");
	for(int i = 0; i < buf_size(files); ++i ) {
		strcat(temp_buff, "structs/");

		char *cleaned_file_name = clean_file_name(files[i]);
		buf_push(fields, printf_struct_start(cleaned_file_name));

		strcat(temp_buff, files[i]);

		file = read_entire_file(temp_buff, NULL);
		temp_file = file;
		open_struct(cleaned_file_name);

		while(*file != '\0') {
			sh_token *field = (sh_token *) malloc(sizeof(sh_token)*3);
			parse_line(field);
			buf_push(fields, field);
			print_struct_field(field);
		}

		close_struct(cleaned_file_name);
		fprintf(generated_file, "\n\n");

		HeapFree(GetProcessHeap(), 0, temp_file);
		buf_push(fields, printf_struct_end());
		free(cleaned_file_name);
		temp_buff[0] = '\0';
	}

	for(int i = 0; i < buf_size(fields); ++i) {
		if(fields[i][0].type == START_STRUCT) { open_function(fields[i][0].name); continue; }
		if(fields[i][0].type == END_STRUCT) { close_function(); continue; }
		print_read_field_be(fields[i]);
	}

	for(int i = 0; i < buf_size(fields); ++i) {
		if(fields[i][0].type == START_STRUCT || fields[i][0].type == END_STRUCT) {
			free(buf_hdr_ptr(fields[i]));
			continue;
		}

		free(fields[i][0].name);
		free(fields[i][1].name);
		free(fields[i][2].name);
		free(fields[i]);
	}	

	for(int i = 0; i < buf_size(files); ++i) {
		free(files[i]);
	}

	free(buf_hdr_ptr(fields));
	free(buf_hdr_ptr(files));
	
	fprintf(generated_file, "#endif");
	fclose(generated_file);
	return 0;
}
