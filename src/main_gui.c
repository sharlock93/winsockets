// Gui components
//	- Input
//	    - have text input
//	- buttons
//	    - button with maybe a call back?
//	- Layout
//	    - possible grid layout?

/*
   1. Text Input widget:
	1.1 [  ] move cursor position, probably gap buffer.
	1.2 [  ] gui - visible cursor, draw a rect
	1.3 [  ] keyboard input for arrow keys to move cursor position
	1.4 [  ] font reading, font for non monospaces.
*/

// #define GL_GLEXT_PROTOTYPES

// #define _DEBUG
// #define _CRTDBG_MAP_ALLOC
// #define _CRTDBG_MAP_ALLOC_NEW
// #include <crtdbg.h>
// #include <stdlib.h>

#include <Windows.h>
#include <Windowsx.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <GL/gl.h>
#include <assert.h>
#include <math.h>

void begin_time_block(char* name);
void end_time_block(char* name);
#include "table_read_utils.c"
#include "sh_tools.c"
#include "sh_simple_vec_math.c"
#include "wglext.h"
#include "glcorearb.h"
#include "gl_load_funcs.h"

#define STB_TRUETYPE_IMPLEMENTATION  // force following include to generate implementation
#include "stb_truetype.h"


#define WSIZE 1920
#define HSIZE 1080 

#define DEBUG 1
#define TIME_BLOCK

int debug = 0;
float debug_resize = 1;

#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))

typedef unsigned __int8 uint8;
typedef unsigned __int16 uint16;
typedef unsigned __int32 uint32;
typedef unsigned __int64 uint64;

typedef __int8  int8;
typedef __int16 int16;
typedef __int32 int32;
typedef __int64 int64;

typedef struct color {
	float r;
	float g;
	float b;
	float a;
} color;

typedef union {
	struct {
		float x;
		float y;
		float z;
		float w;
	};

	float _d[4];
} pos4;

typedef struct rect {
	float left;
	float bottom;
	float width;
	float height;
} rect;

typedef struct line {
	float x1;
	float y1;
	float x2;
	float y2;
} line;

typedef union {
	struct {
		pos4 a;
		pos4 b;
		pos4 c;
		pos4 d;
	};

	float m[16];
}  mat4;

typedef struct sh_image {
	int32 x;
	int32 y;
	int32 size;
	uint8 *mem;
} sh_image;

typedef struct sh_texture {
	int32 gl_tex_id;
	sh_image bitmap;
} sh_texture;

typedef struct time_info {
	uint64 tick;
	uint64 delta_tick;
	
	uint64 time_nano;
	uint64 time_micro;
	uint64 time_milli;
	float  time_sec;
	
	uint64 delta_time_nano;
	uint64 delta_time_micro;
	uint64 delta_time_milli;
	float delta_time_sec;


	uint64 tick_per_sec;
	uint64 start_tick;
} time_info;

typedef enum draw_type {
	NORMAL_DRAW,
	INDEX_DRAW,
	NORMAL_DRAW_TEX
} draw_type;

typedef struct draw_object {
	sh_vec2* points;
	int32 *indices;
	size_t size;
	int32 vertex_count;
	int32 index_count;
	color c;
	GLenum command;
	int32 program;
	int32 type;
	draw_type d_type;
	sh_texture tex;
} draw_object;

typedef struct button {
	bool pressed;
	bool released;
	bool down;
} button;

typedef struct mouse {
	button left_button;
	sh_vec2 mouse_position;
} mouse;

typedef struct gui_state {
	void* active;
} gui_state;

typedef struct text_input {
	char *text;
	int cursor;
} text_input;


typedef sh_vec2 pos2;

typedef struct sh_reloadable_shader {
	FILETIME last_write;
	int32 type;
	char *file_name;
	int32 handle;
	int32 needs_reload;
} sh_reloadable_shader;

typedef struct gl_state {
	sh_reloadable_shader *shaders;
	int32 texture_prog;
	int32 no_texture_prog;
} gl_state;

typedef struct keyboard_button {
	short pressed;
	short released;
	short pressed_once;
} keyboard_button;

typedef struct intersection {
	line *edge;
	float intersect;
	int scanline;
	float pixel;
	float next_pixel;
	float slope;
} intersection; 

typedef struct sh_time_block {
	char* name;
	int32 name_size;
	int64 start;
	int64 end;
	int64 counter;
	int64 sum;
	int32 level;
} sh_time_block;

keyboard_button buttons[256] = {0};

BYTE keyboard_state[256] = {0};



//===================== globals

#define STEPS 30
// float step_val = 1.0/STEPS;
//


font_directory *font = NULL;
stbtt_fontinfo stb_font;

uint8* ttf_file = NULL;
HDC hdc;
HWND wn;
GLuint vbo;
GLuint ibo;
int32 tex_buff;
sh_texture fnt_tx;
draw_object* obj_stack = NULL;
mouse m;
gui_state gl_ui_state;
int quit = 0;
time_info gl_time;
mat4 screen_scale;
char *input_buffer = NULL;
int gl_font_size = 32;
text_input message = {0};

sh_time_block *time_blocks = NULL;
int32 current_level = 0;


gl_state gl_program_state = {0};
//====================


mat4 ortho(float left, float right, float bottom, float top, float pnear, float pfar) {
	float width = (right-left);
	float height = (top - bottom);
	mat4 m = {
		2.0 / (width) , 0            , 0                    , -(right + left)   / (width)  ,
		0             , 2 / (height) , 0					, -(top   + bottom) / (height),
		0             , 0            , -2 / (pfar - pnear)  , -(pfar + pnear) / (pfar - pnear)  ,
		0             , 0			 , 0                    , 1 };
	return m;
}


uint8 safe_add_u8(uint8 a, uint8 b) {
	int c = a + b;
	if( c > 255) {
		return 255;
	}

	return a + b;
}

mat4 ortho_test(float left, float right, float bottom, float top, float pnear, float pfar) {
	float width = (right-left);
	float height = (top - bottom);
	mat4 m = {0};


	m.a.x = 2.0/(width);
	m.b.y = 2.0/(height);
	m.c.z = -2.0/(pfar - pnear);

	m.a.w = - (right + left) / (right - left);
	m.b.w = - (top + bottom) / (top - bottom);
	m.c.w = - (pfar + pnear) / (pfar - pnear);
	m.d.w = 1;
	return m;
}

uint8* read_file(const char *filename, size_t *size) {
	size_t cur_size = 0;

	HANDLE file = CreateFile(
			filename,
			GENERIC_READ, FILE_SHARE_READ, NULL,
			OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL,
			NULL);
	 cur_size = GetFileSize(file, NULL);

	uint8_t* mem = NULL;

	if(cur_size != INVALID_FILE_SIZE) {
		mem = (uint8_t*)malloc(cur_size+1);
		DWORD read = 0;
		ReadFile(file, mem, cur_size, &read, NULL);
		mem[cur_size] = 0;
		assert(read == cur_size);

		if(size) {
			*size = cur_size;
		}
	}

	CloseHandle(file);

	return mem;
}

int search_block_time(char* name) {
	for(int i = 0; i < buf_len(time_blocks); ++i) {
		if(strcmp(name, time_blocks[i].name) == 0) {
			return i;
		}
	}
	return -1;
}

void begin_time_block(char* name) {
#ifdef TIME_BLOCK
	int index = search_block_time(name);

	if(index > -1) {
		sh_time_block *time_block = time_blocks + index;
		LARGE_INTEGER li;
		QueryPerformanceCounter(&li);
		time_block->start = li.QuadPart;
		time_block->counter++;
	} else {
		sh_time_block time_block = {0};
		int length = strlen(name)+1;
		time_block.name = (char*)malloc(length);
		memcpy(time_block.name, name, length);
		time_block.name_size = length;
		
		LARGE_INTEGER li;
		QueryPerformanceCounter(&li);
		time_block.start = li.QuadPart;
		time_block.level = current_level++;
		buf_push(time_blocks, time_block);
	}


#endif
}


void end_time_block(char *name) {
#ifdef TIME_BLOCK
	int index = search_block_time(name);

	if(index > -1) {
		sh_time_block *time_block = time_blocks + index;
		LARGE_INTEGER li;
		QueryPerformanceCounter(&li);
		time_block->end = li.QuadPart;
		time_block->sum += (time_block->end - time_block->start);
		current_level = time_block->level;
	}

#endif
}



sh_texture sh_create_texture(int width, int height, int type) {
	// begin_time_block(__func__);
	sh_texture texture = {0};
	texture.bitmap.x = width;
	texture.bitmap.y = height;
	texture.bitmap.size = width*height*type;
	texture.bitmap.mem = (uint8 *) calloc(1, width*height*type);
	glGenTextures(1, &texture.gl_tex_id);

	// end_time_block(__func__);
	return texture;
}

void sh_destroy_texture(sh_texture *tex) {
	if(tex->bitmap.mem) {
		free(tex->bitmap.mem);
		tex->bitmap.mem = NULL;
	}

	if(tex->gl_tex_id > 0) {
		glDeleteTextures(1, &tex->gl_tex_id);
		tex->gl_tex_id = 0;
	}
}

int32 sh_create_shader(char *file_name, int32 type) {
	char *shader_source = read_file(file_name, NULL);
	int32 shader = glCreateShader(type);
	glShaderSource(shader, 1, &shader_source, NULL);
	glCompileShader(shader);
	
	GLint shader_compile_status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &shader_compile_status);

	free(shader_source);
	if(shader_compile_status != GL_TRUE) {
		int log_size;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_size);
		char *log = malloc(log_size);
		glGetShaderInfoLog(shader, log_size, &log_size, log);
		printf("shader (%s) has errors: %s\n", file_name, log);
		free(log);
		return 0;
	}

	return shader;
}

sh_reloadable_shader sh_create_reloadable_shader(char *file_name, int32 type) {
	sh_reloadable_shader shader = {0};

	shader.handle = sh_create_shader(file_name, type);

	if(shader.handle == 0) {
		printf("error creating reloadable shader.\n");
	} else {
		int32 file_name_size = strlen(file_name)+1;
		shader.file_name = malloc(file_name_size);
		CopyMemory(shader.file_name, file_name, file_name_size);
		shader.type = type;
		shader.last_write = sh_get_file_last_write(shader.file_name);
	}

	
	return shader;
}

int32 sh_create_program(int32 *shaders, int32 shader_count) {
	int32 program = glCreateProgram();
	for(int i = 0; i < shader_count; ++i) {
		glAttachShader(program, shaders[i]);
		printf("shader %d\n", shaders[i]);
	}
	glLinkProgram(program);

	int32 program_link_status;
	glGetProgramiv(program, GL_LINK_STATUS, &program_link_status);

	if(program_link_status != GL_TRUE) {
		int32 log_length = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &log_length);

		char *log = malloc(log_length);
		glGetProgramInfoLog(program, log_length, &log_length, log);
		printf("program linking error(%d): %s\n", __LINE__, log);
		free(log);
		return 0;
	}

	return program;
}



void init_opengl() {

	GLuint vao = 0;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	sh_reloadable_shader vs = sh_create_reloadable_shader("../asset/shader/vertex_shader.glsl", GL_VERTEX_SHADER);
	sh_reloadable_shader fg = sh_create_reloadable_shader("../asset/shader/frag_text.glsl", GL_FRAGMENT_SHADER);
	sh_reloadable_shader fg2 = sh_create_reloadable_shader("../asset/shader/frag.glsl", GL_FRAGMENT_SHADER);


	buf_push(gl_program_state.shaders, vs);
	buf_push(gl_program_state.shaders, fg);
	buf_push(gl_program_state.shaders, fg2);

	int32 shaders[2] = {vs.handle, fg.handle};
	gl_program_state.texture_prog = sh_create_program(shaders, 2);
	shaders[1] = fg2.handle;
	gl_program_state.no_texture_prog = sh_create_program(shaders, 2);

	glUseProgram(gl_program_state.no_texture_prog);

	screen_scale = ortho_test(0,WSIZE, 0, HSIZE, -1.0, 1.0);
	// screen_scale = ortho(-WSIZE/2.0,WSIZE/2.0, -HSIZE/2.0, HSIZE/2.0, -1.0, 1.0);
	glUniformMatrix4fv(2, 1, GL_TRUE, screen_scale.m);
	glViewport(0, 0, WSIZE, HSIZE);

	// int32 buffers[] = {vbo, ibo};
	// glGenBuffers(sizeof(buffers)/sizeof(buffers[0]), buffers);
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ibo);
	// glGenTextures(1, &tex_buff);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glPointSize(4);
}



int point_in_rect(rect r, sh_vec2 p) {
	int right = r.left + r.width;
	int top = r.bottom + r.height;

	if( p.x < right &&
		p.x > r.left &&
		p.y < top &&
		p.y > r.bottom
	  ) return 1;

	return 0;
}


void draw_rectangle(rect r, color c) {
	draw_object o = {0};

	pos2* points = NULL;

	buf_push(points, (pos2){r.left, r.bottom});
	buf_push(points, (pos2){r.left, r.bottom + r.height});
	buf_push(points, (pos2){r.left + r.width ,r.bottom + r.height});

	buf_push(points, (pos2){r.left, r.bottom});
	buf_push(points, (pos2){r.left + r.width, r.bottom + r.height});
	buf_push(points, (pos2){r.left + r.width, r.bottom});

	o.vertex_count = buf_len(points);
	o.size = sizeof(pos2)*o.vertex_count;
	o.points = points;
	o.c = c;
	o.command = GL_TRIANGLES;
	o.program = gl_program_state.no_texture_prog;

	buf_push(obj_stack, o);
}


void draw_rectangle_outline(rect r, color c) {
	draw_object o;

	pos2* points = NULL;

	buf_push(points, (pos2){r.left, r.bottom});
	buf_push(points, (pos2){r.left, r.bottom + r.height});
	buf_push(points, (pos2){r.left + r.width, r.bottom + r.height});
	buf_push(points, (pos2){r.left + r.width, r.bottom});

	o.vertex_count = buf_len(points);
	o.size = sizeof(pos2)*o.vertex_count;
	o.points = points;
	o.c = c;
	o.command = GL_LINE_LOOP;
	o.program = gl_program_state.no_texture_prog;

	buf_push(obj_stack, o);
}

void text_input_insert(text_input *input, char c) {


	if(c == 8) {
		input->cursor--;
		if(input->cursor <= 0) input->cursor = 0;
	}  else {
		input->text[input->cursor++] = c;
	}

	input->text[input->cursor] = '\0';
}

char temp_debug[256];
void draw_line(line l, color c) {
	draw_object o;

	pos2* points = NULL;


	buf_push(points, (pos2){l.x1, l.y1});
	buf_push(points, (pos2){l.x2, l.y2});

	o.vertex_count = buf_len(points);
	o.size = sizeof(pos2)*2;
	o.points = points;
	o.c = c;
	o.command = GL_LINE_LOOP;
	o.program = gl_program_state.no_texture_prog;

	buf_push(obj_stack, o);
}

void draw_line_point(pos2 p1, pos2 p2, color c) {
	draw_object o;

	pos2* points = NULL;

	buf_push(points, (pos2){p1.x, p1.y});
	buf_push(points, (pos2){p2.x, p2.y});

	o.vertex_count = buf_len(points);
	o.size = sizeof(pos2)*2;
	o.points = points;
	o.c = c;
	o.command = GL_LINES;
	o.program = gl_program_state.no_texture_prog;

	buf_push(obj_stack, o);
}

void draw_line_font_point(sh_font_point p1, sh_font_point p2, color c) {
	pos2 p1_p = {p1.x, p2.y};
	pos2 p2_p = {p2.x, p2.y};

	draw_line_point(p1_p, p2_p, c);
}

void draw_point(pos2 p1, color c) {
	draw_object o;

	pos2* points = NULL;
	buf_push(points, (pos2){p1.x, p1.y});
	o.vertex_count = buf_len(points);
	o.size = sizeof(pos2);
	o.points = points;
	o.c = c;
	o.command = GL_POINTS;
	o.program = gl_program_state.no_texture_prog;
	buf_push(obj_stack, o);
}

void draw_points(pos2 *points, color c) {
	draw_object o;
	// pos2* points = NULL;
	// buf_push(points, (pos2){p1.x, p1.y});
	o.vertex_count = buf_len(points);
	o.size = sizeof(pos2)*o.vertex_count;
	o.points = points;
	o.c = c;
	o.command = GL_POINTS;
	o.program = gl_program_state.no_texture_prog;
	buf_push(obj_stack, o);
}

int print_out = 1;

void draw_texture_quad(rect *r, sh_texture tex, color c) {
	draw_object o = {0};

	pos2 *points = NULL;
	buf_push(points, (pos2) {r->left, r->bottom});
	buf_push(points, (pos2) {r->left, r->bottom+r->height});
	buf_push(points, (pos2) {r->left+r->width, r->bottom+r->height});


	buf_push(points, (pos2) {r->left, r->bottom});
	buf_push(points, (pos2) {r->left+r->width, r->bottom+r->height});
	buf_push(points, (pos2) {r->left+r->width, r->bottom});

	buf_push(points, (pos2){0, 0});
	buf_push(points, (pos2){0, 1});
	buf_push(points, (pos2){1, 1});

	buf_push(points, (pos2) {0, 0});
	buf_push(points, (pos2) {1, 1});
	buf_push(points, (pos2) {1, 0});

	int n = buf_len(points);
	print_out = 0;
	o.points = points;
	o.vertex_count = 6;
	o.size = buf_len(points)*sizeof(pos2);
	o.c = c;
	o.command = GL_TRIANGLES;
	o.program = gl_program_state.texture_prog;

	o.tex.bitmap.x = tex.bitmap.x;
	o.tex.bitmap.y = tex.bitmap.y;
	o.tex.bitmap.size = tex.bitmap.size;
	o.tex.gl_tex_id = 0;
	o.tex.bitmap.mem = (char *)malloc(tex.bitmap.size);

	for(int i = 0; i < tex.bitmap.size; ++i) {
		o.tex.bitmap.mem[i] = tex.bitmap.mem[i];
	}

	o.d_type = NORMAL_DRAW_TEX;
	buf_push(obj_stack, o);
}


void render_baseline() {
	line base_line = {-WSIZE, 0, WSIZE, 0};
	color baseline_color = {1, 1, 0, 1};
	draw_line(base_line, baseline_color);
	draw_line((line){0, -HSIZE, 0, HSIZE}, baseline_color);
}

void draw_line_index(pos2* points, int* indices, color c) {
	draw_object o;

	o.points = points;
	o.indices = indices;
	o.d_type = INDEX_DRAW;
	o.vertex_count = buf_len(points);
	o.index_count = buf_len(indices);
	o.size = sizeof(pos2)*o.vertex_count;
	o.c = c;
	o.command = GL_LINES;
	o.program = gl_program_state.no_texture_prog;

	buf_push(obj_stack, o);
}

void render_grid_line(rect *r, int size, color c) {
	float width_div = r->width/size;	
	float height_div = r->height/size;
	float origin_x = r->left;
	float origin_y = r->bottom;

	pos2 a = {origin_x, origin_y};
	pos2 *points = NULL;

	for(int i = 0; i <= size; ++i) {
		if( i%size == 0 )  {
			for(int j = 0; j <= size; ++j) {
				buf_push(points, a);
				a.x += width_div;
			}
		} else {
			buf_push(points, a);
			a.x += r->width;
			buf_push(points, a);
		}
		a.x = origin_x;
		a.y += height_div;
	}


	int indi_num = 2*(size+1);

	int *indices = NULL;
	// bool even = size%2 == 0;
	int left_start = 0;
	int right_start = size;

	for(int i = 0; i <= size; i++) {
		buf_push(indices, i);
		buf_push(indices, 3*size-1 + i);

		if(i == 1) {
			left_start = size + 1;
			buf_push(indices, left_start);
		} else {
			buf_push(indices, left_start);
		}

		left_start += 2;
		
		if(i == (size)) {
			right_start = left_start + size - 2;
			buf_push(indices, right_start);
		} else {
			buf_push(indices, right_start);
			right_start += 2;
		}

	}

	draw_line_index(points, indices, c);
}

rect r = {
	-250, -250, 500, 500 
};


void sh_vec_scalar(pos2 *p, float val) {
	p->x *= val;
	p->y *= val;
}

pos2 sh_vec_scalar_new(pos2 *p, float val) {
	return (pos2){p->x*val, p->y*val};
}

pos2 sh_vec_sub(pos2 *p1, pos2 *p2) {
	return (pos2){p2->x - p1->x, p2->y - p1->y};
}

pos2 sh_vec_add(pos2 *p1, pos2 *p2) {
	return (pos2){p2->x + p1->x, p2->y + p1->y};
}



void draw_triangle_point(pos2 a, pos2 b, color c, float width) {
	// float resize_n = 1;
	// pos2 n = sh_vec_perpendicular(&b, &a);
	// float len = pos2_length(n);
	// if(len == 0) return;
	// n.x /= len;
	// n.y /= len;
	// printf("%f\n", len);
	// n.x *= resize_n;
	// n.y *= resize_n;
	// float len2 = pos2_length(n);
	// pos2 n_r = {-1*n.x, -1*n.y};

	
	// draw_object o;

	// pos2* points = NULL;

	// buf_push(points, (pos2){a.x + n_r.x, a.y + n_r.y});
	// buf_push(points, (pos2){a.x + n.x, a.y + n.y});
	// buf_push(points, (pos2){b.x + n.x, b.y + n.y});

	// buf_push(points, (pos2){a.x + n_r.x , a.y + n_r.y});
	// buf_push(points, (pos2){b.x + n.x, b.y + n.y});
	// buf_push(points, (pos2){b.x + n_r.x, b.y + n_r.y});


	// o.vertex_count = buf_len(points);
	// o.size = sizeof(pos2)*o.vertex_count;
	// o.points = points;
	// o.c = c;
	// o.command = GL_TRIANGLES;
	// o.program = no_texture_prog;

	// buf_push(obj_stack, o);
}


// void draw_bezier_curve_quad(pos2 p1, pos2 p2, pos2 p3, color c) {
// 	pos2 prev_point = p1;
	
// 	for(int i = 0; i < STEPS; ++i) {
// 		float t = step_val*i;

// 		pos2 p1_mod = sh_vec_scalar_new(&p1, (1.0f-t)*(1.0f-t));
// 		pos2 p2_mod = sh_vec_scalar_new(&p2, 2.0*(1.0-t)*t);
// 		pos2 p3_mod = sh_vec_scalar_new(&p3, t*t);
// 		pos2 new_point = sh_vec_add(&p1_mod, &p2_mod);
// 		new_point = sh_vec_add(&new_point, &p3_mod);

// 		prev_point = new_point;
// 	}
// 	draw_triangle_point(prev_point, p3, c, 1);
// }

pos2 sh_font_point_mul(sh_font_point *p, float val) {
	return (pos2) {p->x*val, p->y*val};
}

/*  In digital type, the em is a digitally-defined amount of space.
 *  In an OpenType font, the UPM — or em size is usually set at 1000 units.
 *  In TrueType fonts, the UPM is by convention a power of two, generally set to 1024 or 2048.
 *  When the font is used to set type, the em is scaled to the desired point size.
 *  This means that for 10 pt type, the 1000 units for instance get scaled to 10 pt.
 *  So if your uppercase ‘H’ is 700 units high, it will be 7 pt high on a 10 pt type.
 */
pos2 cur = {-1200, -250};
char *text_meowing = "Sharlock93";


int comp_sh_point(const void *a, const void *b) {
	line *edge_a = (line*)a;
	line *edge_b = (line*)b;

	// float a_y = edge_a->y1 > edge_a->y2 ? edge_a->y1 : edge_a->y2;
	// float b_y = edge_b->y1 > edge_b->y2 ? edge_b->y1 : edge_b->y2;

	if(edge_a->y1 > edge_b->y1) {
		return 1;
	} else if(edge_a->y1 < edge_b->y1) {
		return -1;
	}

	return 0;
}

int comp_intersection(const void *a, const void *b) {
	float i = ((intersection*) a)->intersect;
	float j = ((intersection*) b)->intersect;

	if(i > j) return 1;
	if(i < j) return -1;
	return 0;
}

float find_intersect(line *edge, float scanline) {
	begin_time_block(__func__);
	float dy = edge->y2 - edge->y1;
	float dx = edge->x2 - edge->x1;

	float slope = dy/dx;

	if(slope != 0) {
		float x_intersect = ((scanline - edge->y1)/slope + edge->x1);
		end_time_block(__func__);
		return x_intersect;
	}

	end_time_block(__func__);
	return -1;
}





sh_texture temp_texture;
sh_texture stb_texture;

int edge_direction(line *edge) {
	int dx = edge->x2 - edge->x1;
	int dy = edge->y2 - edge->y1;
	if(dx == 0 || dy == 0) return 0;
	
	return dx*dy > 0 ? 1 : -1;
}

int points_start = 0;
int points_end = 0;

line* generate_edges(pos2 *pts, int contour_counts, int *contour_end_index, int *edges_num) {
	// begin_time_block(__func__);
	line *edges = NULL;
	for(int i = 0; i < contour_counts; ++i) {
		int j = i ? contour_end_index[i-1] : 0;
		for(; j < contour_end_index[i]-1; j++) {
			if(pts[j].y == pts[j+1].y) continue;
			buf_push(edges, (line){pts[j].x, pts[j].y, pts[j+1].x, pts[j+1].y});
		}
	}
	*edges_num = buf_len(edges);

	end_time_block(__func__);
	return edges;
}

void tesselate_bezier_quadratic(pos2 **pts, pos2 p0, pos2 p1, pos2 p2, int steps) {
	
	// begin_time_block(__func__);
	pos2 prev_point = p1;
	float step_val = 1.0/steps;
	for(int i = 1; i <= steps; ++i) {
		float t = step_val*i;

		float x = p0.x*(1.0f-t)*(1.0f-t) + p1.x*2.0*(1.0-t)*t + p2.x*t*t;
		float y = p0.y*(1.0f-t)*(1.0f-t) + p1.y*2.0*(1.0-t)*t + p2.y*t*t;
		pos2 new_point = {x, y};//sh_vec_add(&p0_mod, &p1_mod);
		buf_push(*pts, new_point);
	}
	end_time_block(__func__);
}

pos2* generate_points(sh_glyph_outline *glyph, float scale, int tesselate_amount, int *pts_len, int *num_contour, int **contour_end_index) {
	// begin_time_block(__func__);

	int contour_start = 1;
	int first_point_off = 0;

	int x_offset = glyph->p1.x;
	int y_offset = glyph->p1.y;

	*num_contour = glyph->contour_count;
	pos2 *pts = NULL;
	
	for(int j = 0; j < *num_contour; ++j) {
		int i = j != 0 ? glyph->contour_last_index[j-1]+1 : 0;
		int x = i;
		contour_start = 1;

		for(; i < glyph->contour_last_index[j]; ++i) {
			sh_font_point *p = glyph->points + i;;
			pos2 pflt = (pos2){(p->x-x_offset)*scale, (p->y - y_offset)*scale};
			if(p->flag.on_curve) {
				buf_push(pts, pflt);
			} else {
				pos2 mid_p = {0};
				if(!p[1].flag.on_curve) {
					mid_p.x = p->x + ( (p[1].x - p->x)/2 );
					mid_p.y = p->y + ( (p[1].y - p->y)/2 );
				} else {
					mid_p.x = p[1].x;
					mid_p.y = p[1].y;
				}

				mid_p.x -= x_offset;
				mid_p.x *= scale;

				mid_p.y -= y_offset;
				mid_p.y *= scale;

				if(contour_start) {
					first_point_off = 1;
					//we are starting on an off curve
					if(p[1].flag.on_curve) continue; //next point on curve then just move to the next
					buf_push(pts, mid_p); continue; //point after is off curve too so just make a mid out of the two
				}

				int len = buf_len(pts)-1;
				tesselate_bezier_quadratic(&pts, pts[len], pflt, mid_p, tesselate_amount);
			}

			contour_start = 0;
		}

		sh_font_point *p = glyph->points + i;
		pos2 pflt = (pos2){(p->x-x_offset)*scale, (p->y - y_offset)*scale};
		if(first_point_off) {
			if(p->flag.on_curve)  {
				buf_push(pts, pflt);
			} else {
				pos2 mid_p = (pos2){
					(p->x + ((glyph->points[x].x - p->x)/2) - x_offset)*scale,
					(p->y + ((glyph->points[x].y - p->y)/2) - y_offset)*scale
				};
				tesselate_bezier_quadratic(&pts, pts[buf_len(pts)-1], pflt , mid_p, tesselate_amount);
			}
		} else {
			buf_push(pts, pflt);
		}

		int last_point_contour = j ? (*contour_end_index)[j-1] : 0;
		buf_push(pts, pts[last_point_contour]); //add the first point again so we have an actual loop
		buf_push(*contour_end_index, buf_len(pts)); //mark the end of the contour
		first_point_off = 0;
	}

	end_time_block(__func__);
	return pts;
}

void rasterize_glyph(line *edges, int num_edges, unsigned char *mem, int m_size, int w, int h) {
	begin_time_block(__func__);

	if(!edges) return;

	begin_time_block("sorting edges");
	qsort(edges, num_edges, sizeof(edges[0]), comp_sh_point);
	end_time_block("sorting edges");

	memset(mem, 0, m_size);

	intersection *edge_intersection = NULL;

	//@mem(temp_texture.bitmap.mem, UINT8, 1, temp_texture.bitmap.x, temp_texture.bitmap.y, temp_texture.bitmap.x)
	float offset = 0.0;
	int div_scanline = 5;
	float step_per_div = 255.0/div_scanline;
	float step_per_line = 1.0/div_scanline;
	float alpha = step_per_div;
	int from_edge = 0;
	
	for(int i = 0; i < h; ++i) {
		begin_time_block("raster start");
		int scanline = i;
		for(int n = 0; n < div_scanline; ++n) {
			begin_time_block("div_scanline");
			float scanline_offset = scanline + n*step_per_line;
			begin_time_block("check edges");

			printf("edges: %d\n", num_edges);
			for(int j = from_edge; j < num_edges; ++j) {
				begin_time_block("detect_edges");
				line *edge = edges + j;

				float bigger_y = MAX(edge->y1, edge->y2);
				float smaller_y = MIN(edge->y1, edge->y2);

				if(scanline_offset > bigger_y ) {
					from_edge = j;
					end_time_block("detect_edges");
					break;
				}
				if( smaller_y > scanline_offset) {
					end_time_block("detect_edges");
					from_edge = j;
					break;
				}

				// begin_time_block("actual_intersection_found");
				float x_intersect = find_intersect(edge, scanline_offset);

				if(x_intersect < 0) {
					x_intersect = MIN(edge->x1, edge->x2);
				}

				float max_x = MAX(edge->x1, edge->x2);
				if(x_intersect >= max_x) {
					x_intersect = max_x;
				}

				float pixel_start = (int)x_intersect;
				float next_pixel = pixel_start + 1.0;

				intersection intrsct = {
					.edge = edge,
					.intersect = x_intersect,
					.pixel = pixel_start,
					.next_pixel = next_pixel,
				};

				buf_push(edge_intersection, intrsct);
				// end_time_block("actual_intersection_found");
				end_time_block("detect_edges");
			}

			end_time_block("check edges");

			begin_time_block("sort intersection");
			qsort(edge_intersection, buf_len(edge_intersection), sizeof(intersection), comp_intersection);
			end_time_block("sort intersection");

			unsigned char *current_scanline = mem + scanline*w;

			begin_time_block("filling the intersection");
			for(int m = 0; m < buf_len(edge_intersection); m += 2) {

				float f_pixel = edge_intersection[m].intersect;
				int first_pixel_index =(int)f_pixel;

				float l_pixel = edge_intersection[m+1].intersect;
				int last_pixel_index = (int)l_pixel;

				//both pixel are the same
				if(first_pixel_index == last_pixel_index) {
					float cover = l_pixel - f_pixel;
					current_scanline[first_pixel_index] = safe_add_u8(alpha*cover, current_scanline[first_pixel_index]);
					continue;
				}

				if(first_pixel_index < 0) {
					float cover = f_pixel - (first_pixel_index + 1);
					first_pixel_index = 0;
					current_scanline[first_pixel_index] = safe_add_u8(alpha*cover, current_scanline[first_pixel_index]);
				} else {
					float cover = -f_pixel + (first_pixel_index + 1) ;
					current_scanline[first_pixel_index] = safe_add_u8(alpha*cover, current_scanline[first_pixel_index]);
				}

				if(last_pixel_index >= w) {
					last_pixel_index = w-1;
					float cover = l_pixel - last_pixel_index;
					// current_scanline[last_pixel_index] += alpha*cover;
					current_scanline[last_pixel_index] = safe_add_u8(alpha*cover, current_scanline[last_pixel_index]);
				} else {
					float cover =  l_pixel - last_pixel_index ;
					current_scanline[last_pixel_index] = safe_add_u8(alpha*cover, current_scanline[last_pixel_index]);
				}

				for(int j = first_pixel_index + 1; j < last_pixel_index; ++j) {
					if(j < 0) {
						printf("whut\n");
					} else {
						if(j > w) {
							j = w - 1;
						}
					}
					current_scanline[j] = safe_add_u8(alpha, current_scanline[j]);
				}

			}
			end_time_block("filling the intersection");


			// if(debug) {
			// 	float y = (scanline_offset)*debug_resize;
			// 	pos2 p1 = {0 + WSIZE/2, y+HSIZE/2};
			// 	pos2 p2 = {(w)*debug_resize+WSIZE/2, y+HSIZE/2};
			// 	draw_line_point(p1, p2, (color){0, 0.5, 0, 1});

			// 	for(int i = 0; i < buf_len(edge_intersection); ++i) {
			// 		intersection *m = edge_intersection + i;
			// 		pos2 p = {( m->intersect)*debug_resize+ WSIZE/2, y + HSIZE/2};
			// 		draw_point(p, (color){1, 0, 0.2, 1});
			// 		line l = {
			// 			m->edge->x1*debug_resize, ( m->edge->y1 )*debug_resize,
			// 			m->edge->x2*debug_resize, ( m->edge->y2 )*debug_resize
			// 		};
			// 		// draw_line(l, (color){1, 0.5, 0.5, 1});
			// 	}

			// 	y = (scanline)*debug_resize;
			// 	p1 = (pos2){0 + WSIZE/2, y+HSIZE/2};
			// 	p2 = (pos2){(w)*debug_resize+WSIZE/2, y+HSIZE/2};
			// 	draw_line_point(p1, p2, (color){1, 0.5, 0, 1});

			// }

			buf_clear(edge_intersection);
			end_time_block("div_scanline");
		}
		end_time_block("raster start");
	}

	buf_free(edge_intersection);


	end_time_block(__func__);
}

int do_once = 1;
float offset_scanline = 0;

void render_letter(unsigned char *mem, int m_size, int m_w, int m_h, font_directory *font, char letter, int font_size_pixel) {
	// begin_time_block(__func__);
	sh_glyph_outline g = get_glyph_outline(font, letter); // timed now

	int h = g.p2.y - g.p1.y;
	int as_ds = font->hhea.ascent - font->hhea.descent;
	float ft_scale = (float) font_size_pixel/(as_ds);
	
	int num_contours = 0;
	int *contour_ends_index = NULL;
	int num_edges = 0;
	int pts_len = 0;
	pos2 *pts = generate_points(&g, ft_scale, 5, &pts_len, &num_contours, &contour_ends_index); //the amount of tessilation affects the speed a ton
	line *edges = generate_edges(pts, num_contours, contour_ends_index, &num_edges);
	rasterize_glyph(edges, num_edges, mem, m_size, m_w, m_h);
	
	// if(debug) {
	// 	for(int i = 0; i < num_edges; ++i) {
	// 		draw_line((line){
	// 				( edges[i].x1)*debug_resize+WSIZE/2, ( edges[i].y1 )*debug_resize+HSIZE/2,
	// 				( edges[i].x2)*debug_resize+WSIZE/2, ( edges[i].y2)*debug_resize+HSIZE/2}, (color){0.3, 0, 0, 1});
	// 	}

	// }

	buf_free(contour_ends_index);
	buf_free(edges);
	buf_free(pts);
	sh_free_glyph_outline(&g);
	end_time_block(__func__);
}

float sh_get_scale_for_pixel(font_directory *f, int size_pixel) {
	int as_ds = font->hhea.ascent - font->hhea.descent;
	return (float) size_pixel/as_ds;
}


sh_texture sh_make_texture_letter(font_directory *font, char letter, int size_pixel) {
	// begin_time_block(__func__);
	sh_glyph_rectangle g = get_glyph_rectangle(font, letter);
	int as_ds = font->hhea.ascent - font->hhea.descent;
	float ft_scale = (float) size_pixel/(as_ds);

	int h = g.p2.y - g.p1.y;
	int w = g.p2.x - g.p1.x;

	sh_texture texture = sh_create_texture(ceil( w*ft_scale ), ceil( h*ft_scale ), 1);
	// sh_free_glyph_outline(&g);
	end_time_block(__func__);
	return texture;
}


int size_font = 16; //pixels
char letter = 'j';

float start_point_x = 0;
float start_point_y = 0;


char *big_text = "A";

char buffer_text[1024];


void render_string(char *str, int len, pos2 p, int size_in_pixel, color c) {
	// begin_time_block(__func__);
	sh_texture temp_texture = {0};
	float scale = sh_get_scale_for_pixel(font, size_in_pixel);
	for(int i = 0; i < len; ++i) {
		temp_texture = sh_make_texture_letter(font, str[i], size_in_pixel);
		sh_glyph_outline glyph = get_glyph_outline(font, str[i]);

		render_letter(
				temp_texture.bitmap.mem,
				temp_texture.bitmap.size,
				temp_texture.bitmap.x,
				temp_texture.bitmap.y,
				font, str[i], size_in_pixel);

#if 1
		rect r = {
			p.x + glyph.p1.x*scale,
			p.y + glyph.p1.y*scale,
			temp_texture.bitmap.x,
			temp_texture.bitmap.y};

		sh_glyph_metric m = sh_get_glyph_metric(font, &glyph, scale);
		p.x += m.x_advance;

		draw_texture_quad(&r, temp_texture, c);
#endif
		sh_free_glyph_outline(&glyph);
		sh_destroy_texture(&temp_texture);
	}

	sh_destroy_texture(&temp_texture);
	end_time_block(__func__);
}

void stb_render_string(char *str, pos2 p, int font_size, color c) {

	pos2 p_init = p;
	float scale = stbtt_ScaleForPixelHeight(&stb_font, font_size);
	int ascent = 0;
	int descent = 0;
	int linegap = 0;
	stbtt_GetFontVMetrics(&stb_font, &ascent, &descent, &linegap);

	sh_texture temp_texture = {0};

	for(int i = 0; i < strlen(str); ++i) {

		if(str[i] == '\n') {
			p.y -= (ascent - descent + linegap)*scale;
			p.x = p_init.x;
			continue;
		}

		int xoff = 0;
		int yoff = 0;
		temp_texture.bitmap.mem = stbtt_GetCodepointBitmap(&stb_font, 0, scale, str[i], &temp_texture.bitmap.x, &temp_texture.bitmap.y, &xoff, &yoff);
		temp_texture.bitmap.size = temp_texture.bitmap.x*temp_texture.bitmap.y;

		rect r = {
			p.x - xoff,
			p.y - yoff,
			temp_texture.bitmap.x,
			-temp_texture.bitmap.y
		};


		int x_advance = 0;
		int lef = 0;
		stbtt_GetCodepointHMetrics(&stb_font, str[i], &x_advance, &lef);
		p.x += x_advance*scale;

		draw_texture_quad(&r, temp_texture, c);
		stbtt_FreeBitmap(temp_texture.bitmap.mem, NULL);
	}

}

#define render_interval 0.1
float render_time = render_interval;

void render_time_blocks() {
#if 1
	pos2 pos_time_block = {1, HSIZE-200};
	render_time -= gl_time.delta_time_sec;
	
	if(render_time < 0) {
		int where_to_write = 0;
		for(int i = 0; i < buf_len(time_blocks); ++i) {
			sh_time_block *tb = time_blocks + i;
			double tick_sec = ( (double) ( tb->end - tb->start ))/(gl_time.tick_per_sec);
			double tick_sec_sum = (double)tb->sum/gl_time.tick_per_sec;
			for(int i = 0; i < tb->level; ++i) {
				buffer_text[where_to_write+i] = '|';
			}
			where_to_write += tb->level;

			where_to_write += snprintf(buffer_text + where_to_write, 1024,
					"%s: %.2f / (est: %.2f) (sum: %.2f) (%lld) (%d)\n",
					tb->name, tick_sec*1000000, 1000000*tick_sec*tb->counter, tick_sec_sum*1000000, tb->counter,
					tb->level
					);
		}
		render_time = render_interval;
	}

	stb_render_string(buffer_text, pos_time_block, 16, (color){0, 0, 0, 1});

	for(int i = 0; i < buf_len(time_blocks); ++i) {
		time_blocks[i].counter = 0;
		time_blocks[i].sum = 0;
		current_level = 0;
	}
#endif
}

float update_sec = .2;
float fps = 0;
char fps_buffer[256];

void render_fps() {
	if(update_sec < 0) {
		snprintf(fps_buffer, 256, "FPS: %.0f (%.0ffps)\n", gl_time.delta_time_sec*1000, 1.0/gl_time.delta_time_sec);
		update_sec = .2;
	}

	update_sec -= gl_time.delta_time_sec;
	// render_string(fps_buffer, strlen(fps_buffer), (pos2){0, HSIZE-20}, 20, (color){1, 0, 0, 1});
	stb_render_string(fps_buffer, (pos2){0, HSIZE-20}, 20, (color){1, 0, 1, 1});
}


char *text = "A";

char *test_test = "hello world";
void do_shit() {
	// begin_time_block(__func__);
	render_string(text, strlen(text), (pos2){0, 500}, 40, (color){1, 0, 0, 1});
	end_time_block(__func__);
}


void test_stb() {
	// begin_time_block(__func__);
	stb_render_string(text, (pos2){0, 450}, 40, (color){1, 0, 0, 1});
	// end_time_block(__func__);
}




void render() {
	glClearColor(1, 1, 1, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	do_shit();
	// test_stb();
	render_fps();
	render_time_blocks();
	
	for(draw_object* i = obj_stack; i != buf_end(obj_stack); i++) {
		glUseProgram(i->program);
		glUniformMatrix4fv(2, 1, GL_FALSE, screen_scale.m);
		
		pos2 *points = i->points;
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, i->size, points, GL_DYNAMIC_DRAW);

		switch(i->d_type) {
			case INDEX_DRAW: {
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int32)*i->index_count, i->indices, GL_DYNAMIC_DRAW);
			} break;

			case NORMAL_DRAW_TEX: {
				sh_texture *tex = &i->tex;
				uintptr_t tex_offset = sizeof(pos2)*buf_len(points)/2;
				glEnableVertexAttribArray(3);
				glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 0, (void*)tex_offset);


				int tex_id = 0;

				if(tex->gl_tex_id != 0) {
					tex_id = tex->gl_tex_id;
				} else {
					glGenTextures(1, &tex_id);
					tex->gl_tex_id = tex_id;
				}

				glBindTexture(GL_TEXTURE_2D, tex_id);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);


				glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
				glTexImage2D(
						GL_TEXTURE_2D, 0, GL_RED, tex->bitmap.x, tex->bitmap.y, 0,
						GL_RED, GL_UNSIGNED_BYTE, tex->bitmap.mem);

				glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
			} break;
		}
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

		glUniform4fv(1, 1, &i->c.r);
		if(i->d_type == INDEX_DRAW) {
			glDrawElements(i->command, i->index_count, GL_UNSIGNED_INT, 0);
		} else {
			glDrawArrays(i->command, 0, i->vertex_count);
			// printf("%d\n", i->vertex_count);
		}
		buf_free(i->points);
		sh_destroy_texture(&i->tex);
	}
	buf_clear(obj_stack);
}

float amount = 1.0;

void init_time() {
	LARGE_INTEGER li;
	QueryPerformanceFrequency(&li);
	gl_time.tick_per_sec = li.QuadPart;

	QueryPerformanceCounter(&li);

	gl_time.start_tick = li.QuadPart;
	gl_time.tick = li.QuadPart;

	message.text = calloc(2048, 0);
}

void update_time() {
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);

	uint64 cur_tick = li.QuadPart - gl_time.start_tick;

	gl_time.delta_tick = cur_tick - gl_time.tick;
	gl_time.tick = cur_tick;


	gl_time.time_nano = (1000*1000*1000*gl_time.tick)/(gl_time.tick_per_sec);
	gl_time.time_micro = (1000*1000*gl_time.tick)/(gl_time.tick_per_sec);
	gl_time.time_milli = (1000*gl_time.tick)/(gl_time.tick_per_sec);
	gl_time.time_sec = (float)gl_time.tick/(float)gl_time.tick_per_sec;
	
	gl_time.delta_time_nano = (1000*1000*1000*gl_time.delta_tick)/(gl_time.tick_per_sec);
	gl_time.delta_time_micro = (1000*1000*gl_time.delta_tick)/(gl_time.tick_per_sec);
	gl_time.delta_time_milli = (1000*gl_time.delta_tick)/(gl_time.tick_per_sec);
	gl_time.delta_time_sec = (float)gl_time.delta_tick/(float)gl_time.tick_per_sec;
}

void update_keyboard() {
	GetKeyboardState(keyboard_state);
	for(int i = 0; i < 256; ++i) {
		int cur_state = keyboard_state[i] >> 7;
		buttons[i].released = buttons[i].pressed && cur_state == 0;
		buttons[i].pressed = cur_state;
		buttons[i].pressed_once = buttons[i].released && !buttons[i].pressed;
	}
}

void update() {
	if(buttons[VK_ESCAPE].pressed) {
		quit = 1;
	}
}

void buf_test() {
	int *ll = NULL;

	for(int i = 0; i < 10; ++i) {
		buf_push(ll, i);
	}

	for(int i = 0; i < 10; ++i) {
		assert(ll[i] == i);
	}
	
	draw_object *x = NULL;
	 
	line l = {1, 2, 3, 4};

	draw_object o;
	pos2 points[2];
	points[0].x = l.x1;
	points[0].y = l.y1;
	points[1].x = l.x2;
	points[1].y = l.y2;
	o.vertex_count = 2;
	o.size = sizeof(pos2)*2;
	o.points = points;
	o.c = (color){1, 1, 1, 1};
	o.command = GL_LINES;

	buf_push(x, o);
	buf_push(x, o);
}


void parse_int_test() {
	char *str = "-12 x=21";
	char *strn = "12 y=12";

	int32 x = 0;
	sscanf(str, "%d", &x);
	assert(x == -12);
	sscanf(strn, "%d", &x);
	assert(x == 12);
}

void buf_pass_test(char *buffer) {
	buf_push(buffer, 'x');
	buf_push(buffer, 'x');
	buf_push(buffer, 'x');
	buf_push(buffer, 'x');
}

void tests() {
	buf_test();
	parse_int_test();
	char *meow = NULL;
	buf_pass_test(meow);
	for(int i = 0; i < buf_len(meow); ++i) {
		printf("%d ", i);
	}
	buf_pass_test(meow);
	for(int i = 0; i < buf_len(meow); ++i) {
		printf("%d ", i);
	}
}

void setup_opengl(HWND h_wnd) {
	PIXELFORMATDESCRIPTOR sh_px = {0};
	hdc = GetDC(h_wnd);
	sh_px.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	sh_px.nVersion = 1;
	sh_px.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	sh_px.iPixelType = PFD_TYPE_RGBA;
	sh_px.cColorBits = 32;
	sh_px.cDepthBits = 24;
	sh_px.cStencilBits = 8;

	int index = ChoosePixelFormat(hdc, &sh_px);
	SetPixelFormat(hdc, index, &sh_px);

	HGLRC gl_temp = wglCreateContext(hdc);
	wglMakeCurrent(hdc, gl_temp);

	GLint attr[] = {
		WGL_CONTEXT_MAJOR_VERSION_ARB, 4,
		WGL_CONTEXT_MINOR_VERSION_ARB, 5,
		WGL_CONTEXT_PROFILE_MASK_ARB,
		WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
		0
	};

	load_ext();

	HGLRC gl_c = wglCreateContextAttribsARB(hdc, 0, attr);
	wglMakeCurrent(hdc, gl_c);
	load_ext();

	hdc = GetDC(h_wnd);
	init_opengl();
}

LRESULT sh_proc(HWND h_wnd, UINT msg, WPARAM w_param, LPARAM l_param) {

	LRESULT result = 0; 

	switch(msg) {
		case WM_DESTROY:
			PostQuitMessage(0);
			quit = 1;
			break;

		case WM_MOUSEMOVE:
			POINT p  = (POINT){GET_X_LPARAM(l_param),  GET_Y_LPARAM(l_param)};//(l_param & 0x0000ffff);
			m.mouse_position.x =  p.x - WSIZE;
			m.mouse_position.y = -p.y + HSIZE;
			break;

		case WM_INPUT:
			UINT size;
			GetRawInputData((HRAWINPUT)l_param, RID_INPUT, 0, &size, sizeof(RAWINPUTHEADER));

			void *buffer = malloc(size);

			if(GetRawInputData((HRAWINPUT)l_param, RID_INPUT, buffer, &size, sizeof(RAWINPUTHEADER)) == size) {
				RAWINPUT *raw_input = (RAWINPUT *)buffer;

				if(raw_input->header.dwType == RIM_TYPEMOUSE && raw_input->data.mouse.usFlags == MOUSE_MOVE_RELATIVE) {
					USHORT btn_flags = raw_input->data.mouse.usButtonFlags;

					bool mouse_down = 0;

					if(btn_flags & RI_MOUSE_LEFT_BUTTON_DOWN) {
						mouse_down = true;
					}

					if(btn_flags & RI_MOUSE_LEFT_BUTTON_UP) {
						mouse_down = false;
					}

					bool was_down = m.left_button.down;
					m.left_button.pressed = !was_down && mouse_down;
					m.left_button.released = was_down && !mouse_down;
					m.left_button.down = mouse_down;
				}
			}

			free(buffer);
			result = DefWindowProc(h_wnd, msg, w_param, l_param);
			break;

		case WM_CHAR:
			char ch = (char)w_param;

			buf_push(input_buffer, ch);
			break;
		case WM_KEYDOWN:


			break;
		default:
			result =  DefWindowProc(h_wnd, msg, w_param, l_param);
	}

	return result;
}

void init_raw_input() {
	RAWINPUTDEVICE devs = {0};
	devs.usUsagePage = 0x01;
	devs.usUsage = 0x02;
	devs.hwndTarget = 0;
	
	if(RegisterRawInputDevices(&devs, 1, sizeof(RAWINPUTDEVICE)) == FALSE) {
		printf("couldn't register mouse\n");
	}
}

HWND init_window(HINSTANCE hinst) {
	WNDCLASS wndclass = {0};

	wndclass.style = CS_OWNDC;
	wndclass.lpfnWndProc = (WNDPROC)sh_proc;
	wndclass.hInstance = hinst;
	wndclass.hbrBackground = (HBRUSH) (COLOR_BACKGROUND);
	wndclass.lpszClassName = "sh_gui";
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	RegisterClass(&wndclass);

	RECT win_size = { .left = 0, .right = WSIZE, .top = 0, .bottom = HSIZE };

	AdjustWindowRect(&win_size, WS_OVERLAPPEDWINDOW, FALSE);

	wn = CreateWindow(
			"sh_gui",
			"winsockets",
			WS_VISIBLE | WS_OVERLAPPEDWINDOW,
			0,
			0,
			win_size.right - win_size.left,
			win_size.bottom - win_size.top,
			NULL,
			NULL,
			hinst,
			NULL
			);

	setup_opengl(wn);

	init_time();
	init_raw_input();
	return wn;
}

void reset_mouse() {
	m.left_button.pressed = false;
	m.left_button.released = false;
}

void reset_keyboard() {
	buf_clear(input_buffer);
}

void reload_shader(sh_reloadable_shader *shader) {
	if(shader->needs_reload && shader->handle != 0) {
		char *shader_source = read_file(shader->file_name, NULL);
		printf("%d f: %s %s\n", shader->handle, shader->file_name, shader_source);
		if(shader_source != NULL) {
			shader->needs_reload = 0;
			glShaderSource(shader->handle, 1, &shader_source, NULL);
			glCompileShader(shader->handle);

			GLint shader_compile_status;
			glGetShaderiv(shader->handle, GL_COMPILE_STATUS, &shader_compile_status);
			free(shader_source);
			if(shader_compile_status != GL_TRUE) {
				int log_size;
				glGetShaderiv(shader->handle, GL_INFO_LOG_LENGTH, &log_size);
				char *log = malloc(log_size);
				glGetShaderInfoLog(shader->handle, log_size, &log_size, log);
				printf("shader (%s) has errors: %s\n", shader->file_name, log);
				free(log);
			}

		}
	}
}


void reload_shaders() {
	for(int i = 0; i < buf_len(gl_program_state.shaders); ++i) {
		sh_reloadable_shader *shader = &gl_program_state.shaders[i];
		if(sh_check_file_changed(shader->file_name, &shader->last_write, &shader->last_write)) {
			shader->needs_reload = 1;
		}
	}

	for(int i = 0; i < buf_len(gl_program_state.shaders); ++i) {
		sh_reloadable_shader *shader = &gl_program_state.shaders[i];
		if(shader->needs_reload) {
			reload_shader(shader);
			int program = gl_program_state.no_texture_prog;
			glLinkProgram(program);

			int32 program_link_status;
			glGetProgramiv(program, GL_LINK_STATUS, &program_link_status);

			if(program_link_status != GL_TRUE) {
				int32 log_length = 0;
				glGetProgramiv(program, GL_INFO_LOG_LENGTH, &log_length);

				char *log = malloc(log_length);
				glGetProgramInfoLog(program, log_length, &log_length, log);
				printf("program linking error(%d): %s\n", __LINE__, log);
				free(log);
			}

			program = gl_program_state.texture_prog;
			glLinkProgram(program);

			program_link_status;
			glGetProgramiv(program, GL_LINK_STATUS, &program_link_status);

			if(program_link_status != GL_TRUE) {
				int32 log_length = 0;
				glGetProgramiv(program, GL_INFO_LOG_LENGTH, &log_length);

				char *log = malloc(log_length);
				glGetProgramInfoLog(program, log_length, &log_length, log);
				printf("program linking error(%d): %s\n", __LINE__, log);
				free(log);
			}

		}
	}
}

int main(int argc, char **argv) {
	HWND h = init_window(NULL);
	ttf_file = read_file("envy.ttf", NULL);
	char* file_loc = ttf_file;
	font = sh_init_font(ttf_file);

	stbtt_InitFont(&stb_font, ttf_file, stbtt_GetFontOffsetForIndex(ttf_file,0));
	// free(file_loc);
	memset(keyboard_state, 0, 256);
	update_keyboard();

	wglSwapIntervalEXT(0);
	MSG msg;
	
	while(!quit) {
		reset_mouse();
		reset_keyboard();
		update_time();
		update_keyboard();

		while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		
		if(m.left_button.pressed) {
			printf("weee pressed\n");
		}

		if(m.left_button.released) {
			printf("weee released\n");
		}

		reload_shaders();
		update();
		render();

		SwapBuffers(hdc);
	}

#if 0
	printf("%10s \t %6s \t %2s \t %40s \t %4s \t %40s \t %4s \t %10s\n",
			"address", "length", "freed",
			"malloced at func", "malloced line",
			"freed    at func", "freed    line",
			"realloc_free"
			);
	for(int i = 0; i < buf_len(allocations); ++i) {
		memory_alloc *x = allocations + i;
		if(x->freed == 0)
			printf("%010p \t %06d \t %02d \t %40s \t %04d \t %40s \t %04d \t %02d\n",
					x->address, x->len, x->freed,
					x->func_name, x->line,
					x->free_func_name, x->freed_line, x->realloc_free);
	}
#endif

	return 0;
}
