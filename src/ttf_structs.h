#ifndef SH_TTF_STRUCTS_H
#define SH_TTF_STRUCTS_H
#define FONT_TAG(a, b, c, d) (d<<24|c<<16|b<<8|a)
#define ACNT FONT_TAG('a','c','n','t')
#define ANKR FONT_TAG('a','n','k','r')
#define AVAR FONT_TAG('a','v','a','r')
#define BDAT FONT_TAG('b','d','a','t')
#define BHED FONT_TAG('b','h','e','d')
#define BLOC FONT_TAG('b','l','o','c')
#define BSLN FONT_TAG('b','s','l','n')
#define CMAP FONT_TAG('c','m','a','p')
#define CVAR FONT_TAG('c','v','a','r')
#define CVT_ FONT_TAG('c','v','t',' ')
#define EBSC FONT_TAG('E','B','S','C')
#define FDSC FONT_TAG('f','d','s','c')
#define FEAT FONT_TAG('f','e','a','t')
#define FMTX FONT_TAG('f','m','t','x')
#define FOND FONT_TAG('f','o','n','d')
#define FPGM FONT_TAG('f','p','g','m')
#define FVAR FONT_TAG('f','v','a','r')
#define GASP FONT_TAG('g','a','s','p')
#define GCID FONT_TAG('g','c','i','d')
#define GLYF FONT_TAG('g','l','y','f')
#define GVAR FONT_TAG('g','v','a','r')
#define HDMX FONT_TAG('h','d','m','x')
#define HEAD FONT_TAG('h','e','a','d')
#define HHEA FONT_TAG('h','h','e','a')
#define HMTX FONT_TAG('h','m','t','x')
#define JUST FONT_TAG('j','u','s','t')
#define KERN FONT_TAG('k','e','r','n')
#define KERX FONT_TAG('k','e','r','x')
#define LCAR FONT_TAG('l','c','a','r')
#define LOCA FONT_TAG('l','o','c','a')
#define LTAG FONT_TAG('l','t','a','g')
#define MAXP FONT_TAG('m','a','x','p')
#define META FONT_TAG('m','e','t','a')
#define MORT FONT_TAG('m','o','r','t')
#define MORX FONT_TAG('m','o','r','x')
#define NAME FONT_TAG('n','a','m','e')
#define OPBD FONT_TAG('o','p','b','d')
#define OS_2 FONT_TAG('O','S','/','2')
#define POST FONT_TAG('p','o','s','t')
#define PREP FONT_TAG('p','r','e','p')
#define PROP FONT_TAG('p','r','o','p')
#define SBIX FONT_TAG('s','b','i','x')
#define TRAK FONT_TAG('t','r','a','k')
#define VHEA FONT_TAG('v','h','e','a')
#define VMTX FONT_TAG('v','m','t','x')
#define XREF FONT_TAG('x','r','e','f')
#define ZAPF FONT_TAG('Z','a','p','f')
#include "ttf_generated_structs.h"


typedef struct offset_subtable {
	uint32 scaler_type;
	uint16 num_tables;
	uint16 search_range;
	uint16 entry_selector;
	uint16 range_shift;
} offset_subtable;

typedef struct table_directory_entry {
	union {
		int32 tag;
		char tag_c[4];
	};
	uint32 check_sum;
	uint32 offset;
	uint32 length;
	uint8  *data_ptr;
} table_directory_entry;

typedef struct name_record {
	uint16 platform_id;
	uint16 platform_specific_id;
	uint16 language_id;

	uint16 name_id;
	uint16 length;
	uint16 offset;

	uint8 *data;
} name_record;

typedef struct name_table {
	uint16 format;
	uint16 count;
	uint16 string_offset;
	name_record *records;
	uint8 variable_length[1];
} name_table;

typedef union { char PANOSE[10]; };

typedef struct sh_glyph_offset {
	int32 offset;
	int32 length;
} sh_glyph_offset;

typedef union {
	struct {
		unsigned char on_curve: 1;
		unsigned char x_short_vector: 1;
		unsigned char y_short_vector: 1;
		unsigned char repeat: 1;
		unsigned char x_short_vector_pos: 1;
		unsigned char y_short_vector_pos: 1;
		unsigned char reserved: 2;
	};
	uint8 flags;
} sh_glyph_flag;

// typedef struct sh_simple_glyf {
// 	uint16 *end_pts_cont;
// 	int16 *x_cord;
// 	int16 *y_cord;
// 	sh_glyf_flags *flags;	
// 	uint8 number_of_points;
// } simple_glyf;


typedef struct sh_format4 {
	UInt16  format ; // format  
 	UInt16  length ; // length  
 	UInt16  language ; // language  
 	UInt16  segCountX2 ; // segCountX2  
	UInt16  segCount;
 	UInt16  searchRange ; // searchRange  
 	UInt16  entrySelector ; // entrySelector  
 	UInt16  rangeShift ; // entrySelector  
	UInt16  *endCode;
	UInt16  reservedPad;
	UInt16  *startCode;
	UInt16  *idDelta;
	UInt16  *idRangeOffset;
	UInt16  *glyphIdArray;
	UInt16  glyphIndexCount;
 } sh_format4;

typedef struct sh_loca {
	uint32 *offsets;
	int32 n;
} sh_loca;

typedef struct sh_hor_metric {
	uint16 advance_width;
	int16 left_side_bearing;
} sh_hor_metric;

typedef struct sh_hmtx {
	sh_hor_metric* glyph_metrics;
	FWord *left_side_bearing;
} sh_hmtx;

typedef struct font_directory {
	offset_subtable subtable;
	table_directory_entry *table_directory;
	int32 entry_count;
	uint8 *data;
	sh_cmap cmap;
	sh_maxp maxp;
	sh_loca loca;
	sh_head head;
	sh_hhea hhea;
	sh_hmtx hmtx;
	uint8 *glyph_table;
	sh_format4 f4;

} font_directory;


typedef struct sh_font_point {
	int16 x;
	int16 y;
	sh_glyph_flag flag;
} sh_font_point;

typedef struct sh_fword_point {
	FWord x;
	FWord y;
} sh_fword_point;


typedef struct sh_glyph_outline {
	int32 glyph_index;
	int32 contour_count;
	int32 points_count;
	sh_fword_point p1;
	sh_fword_point p2;
	uint16 instruction_length;
	uint8  *instructions;
	uint16 *contour_last_index;
	sh_font_point *points;	
} sh_glyph_outline;


typedef struct sh_glyph_metric {
	f32 x_advance;
	f32 left_side_bearing;
} sh_glyph_metric;

typedef struct sh_glyph_rectangle {
	sh_fword_point p1;
	sh_fword_point p2;
} sh_glyph_rectangle;


#endif

