// #define sh_malloc(len) sh_malloc_(len, __func__, __LINE__)
// #define sh_realloc(mem, new_len) sh_realloc_( mem, new_len, __func__, __LINE__)
// #define sh_free(mem) sh_free_(mem, __func__, __LINE__)

// #define malloc(len) sh_malloc(len)
// #define realloc(mem, new_len) sh_realloc(mem, new_len)
// #define free(mem) sh_free(mem)

// void* sh_malloc_(size_t mem_len, char* func_name, int line);
// void sh_free_( void* mem, char* func_name, int line);
// void* sh_realloc_(void* mem, size_t new_size, char* func_name, int line);

typedef struct memory_alloc {
	void* address;
	size_t len;
	int freed;
	int realloc_free;

	char *func_name;
	int line;

	char *free_func_name;
	int freed_line;
} memory_alloc;

int allocated_size = 0;

memory_alloc *allocations = NULL;


#if 0
void* sh_malloc_(size_t mem_len, char* func_name, int line) {
	void* new_mem = (void*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, mem_len);
	// memory_alloc new_mem_alloc = {
	// 	.address = new_mem,
	// 	.len = mem_len,
	// 	.freed = 0,
	// 	.realloc_free = 0,
	// 	.func_name = fu
	// }
	// allocations[allocated_size].address = new_mem;
	// allocations[allocated_size].len = mem_len;
	// allocations[allocated_size].freed = 0;
	// allocations[allocated_size].line = line;
	// allocations[allocated_size].func_name = func_name;
	// allocated_size++;
	// buf_push_untrack(allocations, (memory_alloc){new_mem, mem_len, 0, 0, func_name, line, NULL, 0});
	// return new_mem;
}

void sh_free_( void* mem, char* func_name, int line) {
	int freed = 0;
	for(int i = 0; i < buf_len(allocations); ++i) {
		if(mem == allocations[i].address && allocations[i].freed != 1) {
			allocations[i].freed = 1;
			allocations[i].free_func_name = func_name;
			allocations[i].freed_line = line;
			freed = 1;
			break;
		} 
	}

	if(freed == 0) {
		// printf("weird free\n");
		// memory_alloc *m = allocations+allocated_size;
		// m->address = mem;
		// m->len = -1;
		// m->freed = 1;
		// m->free_func_name = func_name;
		// m->freed_line = line;
		buf_push_untrack(allocations, (memory_alloc){mem, -1, 1, 0, NULL, 0, func_name, line});
	}

	HeapFree(GetProcessHeap(), 0, mem);
}

void* sh_realloc_(void* mem, size_t new_size, char* func_name, int line) {

	void* new_mem = HeapReAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, mem, new_size);

	if(new_mem != mem && new_mem != NULL) {
		for(int i = 0; i < buf_len(allocations); ++i) {
			memory_alloc *m = allocations + i;
			if(m->address == mem) {
				m->freed = 1;
				m->realloc_free = 1;
				m->free_func_name = func_name;
				m->freed_line = line;
				break;
			}
		}

		// memory_alloc *x = allocations + allocated_size;
		// x->address = new_mem;
		// x->len = new_size;
		// x->freed = 0;
		// x->line = line;
		// x->func_name = func_name;
		// allocated_size++;
		buf_push_untrack(allocations, (memory_alloc){new_mem, new_size, 0, 0, func_name, line, NULL, 0});
	}

	return new_mem;
}

#endif

#define buf_fit_untrack(b, n) ((n) <= buf_cap(b) ? 0 : ((b) = buf__grow_untrack(b, buf_len(b) + (n), sizeof(*(b)))))
#define buf_push_untrack(b, ...) ( buf_fit_untrack((b), 1 + buf_len(b)), (b)[buf__hdr(b)->size++] = (__VA_ARGS__))

void* buf__grow_untrack(const void *buf, int items, size_t item_size) {
	size_t new_cap = 1 + 2*buf_cap(buf);
	assert(new_cap >= items);
	size_t new_size = sizeof(int)*2 + new_cap*item_size;
	
	bhdr *nhdr = NULL;
	if(buf) {
		nhdr = HeapReAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, buf__hdr(buf), new_size);
		// nhdr = sh_realloc_(buf__hdr(buf), new_size, func_name, line);
	} else {
		// nhdr = sh_malloc_(new_size, func_name, line);
		nhdr = (void*) HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, new_size);
		nhdr->size = 0;
	}

	nhdr->cap = new_cap;
	return ((int*)nhdr) + 2;
}

