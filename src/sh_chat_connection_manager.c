#include "sh_chat_connection_manager.h"

SOCKET init_winsock( WORD s_ver, WSADATA* wsa_data, char* ip, char* port) {
    int init_result = WSAStartup(s_ver, wsa_data);
    SOCKET con_sock = INVALID_SOCKET;

    struct addrinfo* ip_addr;

#ifdef SH_DEBUG
    switch(init_result) {
	case WSASYSNOTREADY:
	    printf("well shit system is not ready!\n");
	    break;
	case WSAVERNOTSUPPORTED:
	    printf("you asked for the wrong version ma dude\n");
	    break;
	case WSAEINPROGRESS:
	    printf("version 1.1 is fucking you up");
	    break;
	case WSAEPROCLIM:
	    printf("you have reached the limites of sockets\n");
	    break;
	case WSAEFAULT:
	    printf("the data passed is wrong");
	    break;
    }
#endif

    if(init_result != 0) { return init_result; }

#ifdef SH_DEBUG
    printf("successfully inited the win socketd:");
    printf("version: %d.%d (%s)\n",
	    wsa_data.wVersion & (0x00FF),
	    wsa_data.wVersion & (0xFF00) >> 8,
	    wsa_data.szSystemStatus
	  );
#endif

    struct addrinfo addr_hint;// = {};
    ZeroMemory(&addr_hint, sizeof(addr_hint));
    addr_hint.ai_family = AF_INET;
    addr_hint.ai_socktype = SOCK_STREAM;
    addr_hint.ai_protocol = IPPROTO_TCP;

    //TODO(sharo): does it even need this shit? this looks like overkill
    getaddrinfo(ip, port, &addr_hint, &ip_addr);

    con_sock = socket(ip_addr->ai_family, ip_addr->ai_socktype, ip_addr->ai_protocol);
    int con_res = bind(con_sock, ip_addr->ai_addr, ip_addr->ai_addrlen);

    if(con_res == SOCKET_ERROR) return INVALID_SOCKET;

    return con_sock;
}

socket_manager* sh_make_socket_manager(int init_capacity, int msg_capacity) {
    //TODO, maybe more error checks?

    socket_manager* mngr = (socket_manager*) HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(socket_manager));
    mngr->cons = (connection_handle* ) HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(connection_handle)*init_capacity);
    mngr->capacity = init_capacity;
    for(int i = 0; i < mngr->capacity; ++i) {
	mngr->cons[i].state = CLOSED;
    }

    //thread starts immedately
    mngr->thread = CreateThread(0, 0, &handle_connection, mngr, 0, &mngr->thread_id); 
    mngr->con_mutex = CreateMutex(NULL, FALSE, NULL);

    msg_queue* m_q = &mngr->m_queue;

    m_q->queue = (chat_msg*) HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(chat_msg)*msg_capacity);
    m_q->capacity = msg_capacity;
    m_q->size = 0;
    m_q->mutex = CreateMutex(NULL, FALSE, NULL);
    m_q->thread = CreateThread(0, 0, &handle_msgs, mngr, 0, 0);
    m_q->head = 0;
    m_q->back = 0;

    return mngr;
}

void sh_sock_add_handle(socket_manager* mngr, connection_handle* handle) {
    assert(mngr->size <= mngr->capacity);
    if(mngr->size >= mngr->capacity ) {
	sh_grow_socket_cap(mngr);
    }

    printf("hello expanding");

    for(int i = 0; i < mngr->capacity; ++i ) {
	printf("ols: %d, ns: %d state: %d, %d\n", (int)mngr->cons[i].socket, (int)handle->socket, mngr->cons[i].state, handle->state);
	if(mngr->cons[i].state == CLOSED)  {
	    mngr->cons[i] = *handle;
	    printf("ols: %d, ns: %d\n", (int)mngr->cons[i].socket, (int)handle->socket);
	    break;
	}
    }

    mngr->size++;
}

void sh_grow_socket_cap(socket_manager* mngr) {
    //grow manager connection handle 
    WaitForSingleObject(mngr->con_mutex, INFINITE);
    void* old_mem = mngr->cons;
    void* new_mem = HeapReAlloc(GetProcessHeap(),  HEAP_ZERO_MEMORY, old_mem, sizeof(connection_handle)*mngr->capacity*GROW_BY_MLT);
    printf("capacity: %d %p\n", mngr->capacity, new_mem);
    if(new_mem != NULL) {
	mngr->cons = (connection_handle*) new_mem;
	mngr->capacity *= GROW_BY_MLT;
	ReleaseMutex(mngr->con_mutex);
    } else {
	ReleaseMutex(mngr->con_mutex);
	return;
    }

    printf("capacity: %d\n", mngr->capacity);
}


//TODO(sharo): maybe not LPVOID directly? assumed for threading
DWORD handle_connection(LPVOID params) {
    socket_manager* mngr = (socket_manager*) params;

    char buffer[256];
    int bytes_gotten = 0;
    int last_e = 0;

    while(true) {
	ZeroMemory(buffer, sizeof(buffer));
	WaitForSingleObject(mngr->con_mutex, INFINITE);
	for(int i = 0; i < mngr->capacity; ++i) {
	    connection_handle* handle = &mngr->cons[i];

	    switch(handle->state) {
		case CLOSED:
		    continue;
		    break;

		case INIT: {

		    bytes_gotten = recv(handle->socket, buffer, MAX_USERNAME_LEN, 0);
		    last_e = WSAGetLastError();
#ifdef SH_DEBUG
		    printf("%d %d\n", bytes_gotten == SOCKET_ERROR, last_e == WSAEWOULDBLOCK);
#endif

		    if(last_e == WSAEWOULDBLOCK) continue;

		    if(bytes_gotten > 0) {
			// sh_validate_user();
			sh_get_db_user(mngr->db_connection, buffer, &handle->user);
#ifdef SH_DEBUG
			printf("--debug: bytes_gotten: %d %s strlen(%zd)--\n", bytes_gotten, buffer, strlen(buffer));
#endif
		    }

		    if(handle->user.user_id != -1) {
			handle->state = ROOM_INIT;
			char* msg = "Please choose your room using /room <room_id>";
			send(handle->socket, msg, strlen(msg), 0);
			ZeroMemory(buffer, sizeof(buffer));
		    }
		} break;

		case ROOM_INIT: {
		    
		} break;

		case OPEN: {
		    bytes_gotten = recv(handle->socket, buffer, sizeof(buffer), 0);
		    last_e = WSAGetLastError();
		    if(bytes_gotten == SOCKET_ERROR && last_e != WSAEWOULDBLOCK) {
			closesocket(handle->socket);
			handle->state = CLOSED;
			mngr->size--;
			/* printf("%d %d socket: %d bytes_gotten: %d\n", last_e,i, (int)handle->socket, bytes_gotten); */
			break;
		    }

		    if(last_e == WSAEWOULDBLOCK) {
			continue;
		    }

		    if(bytes_gotten != 0) {
			chat_msg msg = {buffer, bytes_gotten, 0, 0};
			sh_push_msg(&mngr->m_queue, &msg);
			sh_store_db_msg(mngr->db_connection, &msg, &handle->user);
		    } 
		} break;

	    }

	}
	ReleaseMutex(mngr->con_mutex);
	Sleep(10);
    }
}



