#include <stdio.h>
#include <stdlib.h>

typedef struct {
    char* f_name;
    char* f_type; //same as name but with PFN + UPPERCASE + PROC
    int   f_len;
} function_struct ;


typedef struct func_node {
    function_struct* cur;
    struct func_node* next;
} func_node;


func_node* f_head;

char* print_upper(char *str)
{
    int str_length = strlen(str) + 1 + 3 + 4; //PFN + gl + PROC

    char *func_struct = (char *)calloc(1, str_length);

    func_struct[0] = 'P';
    func_struct[1] = 'F';
    func_struct[2] = 'N';
    func_struct[str_length - 5] = 'P';
    func_struct[str_length - 4] = 'R';
    func_struct[str_length - 3] = 'O';
    func_struct[str_length - 2] = 'C';
    func_struct[str_length - 1] = '\0';

    for(int i = 0; i < str_length - 1 - 3 - 4; ++i) { //one for new line, 3 for pfn 4 for proc
        if(str[i] == '\n') continue;
        *(func_struct + 3 + i) = toupper(*(str + i));
    }

    return func_struct;
}

void add_func_type(func_node** n, char* f) {
    if(n == NULL) return;

    func_node* fn = (func_node*) malloc(sizeof(func_node));
    fn->cur = NULL;
    fn->next = NULL;
    function_struct* fs = (function_struct*) malloc(sizeof(function_struct));

    fs->f_len = strlen(f);
    fs->f_name = (char*) malloc(fs->f_len*sizeof(char));

    strncpy(fs->f_name, f, fs->f_len);
    fs->f_name[fs->f_len-1] = '\0';

    fs->f_type = print_upper(fs->f_name);

    fn->cur = fs;
    fn->next = *n;

    *n = fn;
}

int func_type_exists(func_node* node_head, char* func_name) {
    while(node_head != NULL && node_head->cur != NULL) {
	if(strncmp(node_head->cur->f_name, func_name, node_head->cur->f_len) == 0) {
	    return 1;
	}
	node_head = node_head->next;
    }
    return 0;
}

int main(int argc, char **argv)
{
    char buffer[256];

    FILE *file_in = fopen("funcs_to_load.txt", "r");
    FILE* file_out = fopen("gl_load_funcs.h", "w");
    while (fgets(buffer, sizeof(buffer), file_in)) {
	if(func_type_exists(f_head, buffer)) continue;
	add_func_type(&f_head, buffer);
    }

    func_node* temp = f_head;
    while(temp != NULL) {
	fprintf(file_out, "%.*s %.*s;\n", temp->cur->f_len + 7, temp->cur->f_type, temp->cur->f_len, temp->cur->f_name);
	temp = temp->next;
    }


    fprintf(file_out, "void load_ext() {\n");
    temp = f_head; 
    while(temp != NULL) {
	fprintf(file_out, "\t%.*s = (%.*s) wglGetProcAddress(\"%.*s\");\n", 
		temp->cur->f_len, temp->cur->f_name,
		temp->cur->f_len + 7, temp->cur->f_type,
		temp->cur->f_len, temp->cur->f_name
		);
	temp = temp->next;
    }

    fprintf(file_out, "}\n");


    return 0;
}
