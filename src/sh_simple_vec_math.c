#ifndef SH_SIMPLE_MATH
#define SH_SIMPLE_MATH

typedef struct sh_vec2 {
	float x;
	float y;
} sh_vec2;

void sh_vec2_add(sh_vec2 *r, sh_vec2 *a, sh_vec2 *b);
void sh_vec2_sub(sh_vec2 *r, sh_vec2 *a, sh_vec2 *b);
void sh_vec2_mul(sh_vec2 *r, sh_vec2 *a, float val);
float sh_vec2_dot(sh_vec2 *a, sh_vec2 *b);
void sh_vec2_normal(sh_vec2 *r, sh_vec2 *a);
float sh_vec2_lensq(sh_vec2 *a);
float sh_vec2_len(sh_vec2 *a);
void sh_vec2_normalize(sh_vec2 *r, sh_vec2 *a);

void sh_vec2_add(sh_vec2 *r, sh_vec2 *a, sh_vec2 *b) {
	r->x = a->x + b->x;
	r->y = a->y + b->y;
}

void sh_vec2_sub(sh_vec2 *r, sh_vec2 *a, sh_vec2 *b) {
	r->x = a->x - b->x;
	r->y = a->y - b->y;
}

void sh_vec2_mul(sh_vec2 *r, sh_vec2 *a, float val) {
	r->x = a->x*val;
	r->y = a->y*val;
}


void sh_vec2_normal(sh_vec2 *r, sh_vec2 *a) {
	r->x = -a->y;
	r->y =  a->x;
}

void sh_vec2_normalize(sh_vec2 *r, sh_vec2 *a) {
	float length = sh_vec2_len(a);
	r->x = a->x/length;
	r->y = a->y/length;
}

float sh_vec2_lensq(sh_vec2 *a) {
	return a->x*a->x + a->y*a->y;
}

float sh_vec2_len(sh_vec2 *a) {
	return sqrt(sh_vec2_lensq(a));
}

float sh_vec2_dot(sh_vec2 *a, sh_vec2 *b) {
	return a->x*b->x + a->y*b->y;
}


//debug funcs

void print_sh_vec2(sh_vec2 *p) {
	printf("(x: %f, y: %f)", p->x, p->y);
}

#endif
