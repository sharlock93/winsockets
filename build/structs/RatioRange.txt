uint8 	bCharSet 	Character set (see below).
uint8 	xRatio 	Value to use for x-Ratio
uint8 	yStartRatio 	Starting y-Ratio value.
uint8 	yEndRatio 	Ending y-Ratio value.
