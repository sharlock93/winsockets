int16 	numberOfContours 	If the number of contours is positive or zero, it is a single glyph; If the number of contours less than zero, the glyph is compound
FWord 	xMin 	Minimum x for coordinate data
FWord 	yMin 	Minimum y for coordinate data
FWord 	xMax 	Maximum x for coordinate data
FWord 	yMax 	Maximum y for coordinate data
