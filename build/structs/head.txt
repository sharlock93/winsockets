Fixed 	version 	0x00010000 if (version 1.0)
Fixed 	fontRevision 	set by font manufacturer
uint32 	checkSumAdjustment 	To compute: set it to 0, calculate the checksum for the 'head' table and put it in the table directory, sum the entire font as a uint32_t, then store 0xB1B0AFBA - sum. (The checksum for the 'head' table will be wrong as a result. That is OK; do not reset it.)
uint32 	magicNumber 	set to 0x5F0F3CF5
uint16 	flags 	bit 0 - y value of 0 specifies baseline
uint16 	unitsPerEm 	range from 64 to 16384
longDateTime 	created 	international date
longDateTime 	modified 	international date
FWord 	xMin 	for all glyph bounding boxes
FWord 	yMin 	for all glyph bounding boxes
FWord 	xMax 	for all glyph bounding boxes
FWord 	yMax 	for all glyph bounding boxes
uint16 	macStyle 	bit 0 bold
uint16 	lowestRecPPEM 	smallest readable size in pixels
int16 	fontDirectionHint 	0 Mixed directional glyphs
int16 	indexToLocFormat 	0 for short offsets, 1 for long
int16 	glyphDataFormat 	0 for current format
