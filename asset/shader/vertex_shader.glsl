#version 450
in vec2 vp;
out vec4 out_tex;
layout( location = 1 ) uniform vec4 color;
layout( location = 2 ) uniform mat4 view = mat4(1);
layout( location = 3 ) in vec2 tc;

void main() {
   gl_Position = vec4(vp.x, vp.y, 0, 1)*view;
   out_tex = vec4(tc.x, tc.y, 0, 0);
}

