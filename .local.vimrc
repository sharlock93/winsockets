let &path.="header,src,"
nnoremap <A-m> :AsyncRun build.bat<cr>
nnoremap <A-p> :AsyncStop<cr>
nnoremap <A-r> :AsyncRun run.bat<cr>
nnoremap <F7> :cn<cr>

" set errorformat=\.\.%f|%l| %m
