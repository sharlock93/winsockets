@echo off
:: pushd build
set libs=-lWs2_32 -luser32 -lgdi32 -lOpenGL32
set libs_win=Ws2_32.lib user32.lib gdi32.lib OpenGL32.lib
rem cl /nologo ../src/main_read_ttf.c -I../headers  %libs_win% /Zi
cl /nologo src\main_gui.c -Iheaders /Fe./build/main_gui.exe -Fo./build/main_gui.obj -Fd./build/main_gui.pdb /std:c++14  %libs_win% /Zi /MT
rem cl /nologo  ../src/read_structs.c /I"../headers"  %libs_win% /Zi
cl /nologo src\test.cpp -Iheaders /Fe./build/test.exe -Fo./build/test.obj -Fd./build/test.pdb  %libs_win% /Zi
:: popd
